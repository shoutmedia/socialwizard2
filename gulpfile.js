const { parallel, series, src, dest, watch } = require('gulp');
const sass = require('gulp-sass');
const prefix = require('gulp-autoprefixer');
const minifycss = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const sync = require('browser-sync').create();
const del = require('delete');
const webpackconfig = require('./webpack.config.js');
const webpack = require('webpack');
const gulpWebpack = require('webpack-stream');

const config = {
    url: 'homestead.test',
    port: 3000,
    src: {
        img: 'resources/img/**/*.{png,jpg,gif}',
        js: 'resources/js/**/*.js',
        css: 'resources/sass/**/*.scss',
    },
    dest: {
        img: 'public/img',
        js: 'public/js',
        css: 'public/css',
    },
};

function browsersync(cb) {
    sync.init({
        proxy: config.url,
        port: config.port,
        ui: false,
        online: true,
        logPrefix: 'Arcadia',
        open: false,
    });

    cb();
}

function styles(cb) {
    src(config.src.css)
        .pipe(sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        .pipe(prefix({
            cascade: false,
        }))
        .pipe(dest(config.dest.css))
        .pipe(minifycss())
        .pipe(sync.stream({
            match: '**/*.css',
        }));

    cb();
}

function images(cb) {
    src(config.src.img)
        .pipe(imagemin())
        .pipe(dest(config.dest.img));

    cb();
}

function reload(cb) {
    sync.reload();
    cb();
}

function javascript(cb) {
    src(config.src.js)
        .pipe(gulpWebpack(webpackconfig, webpack))
        .pipe(dest(config.dest.js));

    reload(cb);
}

function cleanup(cb) {
    del([
        config.src.img,
        `${config.src.fonts}*.otf`,
        `${config.src.fonts}*.ttf`,
    ], cb);
}

function monitor(cb) {
    watch(config.src.css, styles);
    watch(config.src.img, { events: ['add'], delay: 500 }, series(images, cleanup));
    watch(config.src.js, javascript);

    cb();
}

exports.default = series(browsersync, parallel(styles, javascript, images), monitor);
