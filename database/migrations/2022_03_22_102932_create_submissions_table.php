<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ref')->index();

            // Step 1
            $table->string('first')->nullable();
            $table->string('last')->nullable();
            $table->string('email')->nullable();
            $table->string('choice')->nullable();
            $table->boolean('accepted')->default(false);

            // Step 2
            $table->string('country')->nullable();
            $table->string('hometown')->nullable();
            $table->string('student_id')->nullable();
            $table->string('level_of_study')->nullable();
            $table->string('program')->nullable();

            // Step 3
            $table->string('originalfile')->nullable();
            $table->boolean('confirmation')->default(false);
            $table->text('location_details')->nullable();
            $table->text('location')->nullable();

            // Step 4
            $table->string('frame')->nullable();
            $table->string('posx')->nullable();
            $table->string('posy')->nullable();
            $table->string('poswidth')->nullable();
            $table->string('posheight')->nullable();

            // Internal
            $table->string('finalphoto')->nullable();
            $table->string('preview')->nullable();
            $table->boolean('is_complete')->default(false);
            $table->unsignedInteger('approved_by')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
