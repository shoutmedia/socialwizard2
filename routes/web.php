<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('welcome');
Route::get('/education', 'PagesController@education')->name('education');
Route::get('/home', 'PagesController@home')->name('home');
Route::get('/experience', 'PagesController@experience')->name('experience');
Route::get('/future', 'PagesController@future')->name('future');
Route::get('/incoming-class', 'PagesController@gallery')->name('gallery');
Route::get('/contest', 'PagesController@contest')->name('contest');

Route::get('begin/{ref?}', 'WizardController@index');
Route::post('begin', 'WizardController@store');
Route::post('uploads', 'UploadsController@store');
Route::get('submission/{ref}', 'PosterPreviewsController@show');

Route::middleware(['web', 'auth'])
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('posters', 'PostersController')->only('index', 'update', 'edit');
        Route::put('approve/{submission}', 'Posters\AddToFeed')->name('poster.approve');
        Route::put('remove/{submission}', 'Posters\RemoveFromFeed')->name('poster.remove');
        Route::delete('deny/{submission}', 'Posters\DeleteEntry')->name('poster.deny');
    });
