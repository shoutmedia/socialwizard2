<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Rapture') }}</title>

        <link rel="stylesheet" href="{{ asset('css/cropper.min.css') }}"/>
        <link rel="stylesheet" href="{{ asset('slick/slick.css') }}"/>
        <link rel="stylesheet" href="{{ asset('slick/slick-theme.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        @stack('styles')

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    </head>
    @hasSection('body_class')
    <body class="@yield('body_class')">
    @else
    <body>
    @endif
        @yield('content')

        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/cropper.min.js') }}"></script>
        <script src="{{ asset('plupload/plupload.full.min.js') }}"></script>
        <script src="{{ asset('js/public.js?20190415') }}"></script>

        @stack('scripts')
    </body>
</html>
