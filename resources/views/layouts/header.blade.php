<header class="header">
    <div class="contained clearfix">
        <div class="slider">
            <div class="shield"></div>
            <div class="caption">
                <span class="left-text">Your</span>
                <div class="center-text">
                    @isset($headings)
                        @foreach ($headings as $heading)
                            <div>{{ $heading }}</div>
                        @endforeach
                    @endisset
                </div>
                <span class="right-text">Matters</span>
            </div>
        </div>
    </div>
    <div class="gradient-overlay"></div>
</header>
