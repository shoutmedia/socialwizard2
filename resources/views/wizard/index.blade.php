@extends('layouts.public')

@section('content')
    @include('layouts.header', [
        'headings' => [
            'Education',
            'Journey',
            'Voice',
            'Dream',
            'Choice',
        ]
    ])

    <div class="poster-wizard bg-white text-blue pt-normal pb-normal">
        <div class="container">
            <form method="post" action="{{ url('begin') }}" class="raw-data"></form>

            <div class="user-steps">
                <form class="step-content step-1 active">
                    <input type="hidden" name="step" value="1">
                    <p class="intro">Welcome the Incoming Class of 2022! Share your Lakehead pride and you’ll have a chance to win some incredible prizes, including free first-year residence plus meal plan! <a class="underline" href="{{ route('contest') }}#prizes" target="_blank">Check out all 10 prizes</a>. Participate in 4 easy steps... start here:</p>

                    <h2 class="step-title">Step 1: Tell us about yourself</h2>
                    <p class="text-center">All fields are required.</p>

                    <div class="errors hidden"></div>

                    <div class="form-field">
                        <label for="">First Name</label>
                        <input type="text" name="firstname" value="{{ old('firstname', $submission ? $submission->first : '') }}">
                    </div>
                    <div class="form-field">
                        <label for="">Last Name</label>
                        <input type="text" name="lastname" value="{{ old('lastname', $submission ? $submission->last : '') }}">
                    </div>
                    <div class="form-field">
                        <label for="">Lakehead Email</label>
                        <input type="email" name="emailaddress" value="{{ old('emailaddress', $submission ? $submission->email : '') }}">
                    </div>
                    <div class="form-field">
                        <label for="">As you begin life as a Lakehead student, we'd like to know what matters to you:</label>
                        <div class="form-field choice font-bold">
                            <span>My</span>
                            <select name="framechoice" class="font-bold">
                                <option value="">- Select One </option>
                                <option value="choice"{{ old('framechoice', $submission ? $submission->choice : '') === 'choice' ? ' selected' : '' }}>Choice</option>
                                <option value="dream"{{ old('framechoice', $submission ? $submission->choice : '') === 'dream' ? ' selected' : '' }}>Dream</option>
                                <option value="education"{{ old('framechoice', $submission ? $submission->choice : '') === 'education' ? ' selected' : '' }}>Education</option>
                                <option value="journey"{{ old('framechoice', $submission ? $submission->choice : '') === 'journey' ? ' selected' : '' }}>Journey</option>
                                <option value="voice"{{ old('framechoice', $submission ? $submission->choice : '') === 'voice' ? ' selected' : '' }}>Voice</option>
                                <option value="curiosity"{{ old('framechoice', $submission ? $submission->choice : '') === 'curiosity' ? ' selected' : '' }}>Curiosity</option>
                                <option value="experience"{{ old('framechoice', $submission ? $submission->choice : '') === 'experience' ? ' selected' : '' }}>Experience</option>
                                <option value="home"{{ old('framechoice', $submission ? $submission->choice : '') === 'home' ? ' selected' : '' }}>Home</option>
                                <option value="family"{{ old('framechoice', $submission ? $submission->choice : '') === 'family' ? ' selected' : '' }}>Family</option>
                                <option value="culture"{{ old('framechoice', $submission ? $submission->choice : '') === 'culture' ? ' selected' : '' }}>Culture</option>
                                <option value="community"{{ old('framechoice', $submission ? $submission->choice : '') === 'community' ? ' selected' : '' }}>Community</option>
                                <option value="wellness"{{ old('framechoice', $submission ? $submission->choice : '') === 'wellness' ? ' selected' : '' }}>Wellness</option>
                                <option value="food"{{ old('framechoice', $submission ? $submission->choice : '') === 'food' ? ' selected' : '' }}>Food</option>
                                <option value="comfort"{{ old('framechoice', $submission ? $submission->choice : '') === 'comfort' ? ' selected' : '' }}>Comfort</option>
                                <option value="spirit"{{ old('framechoice', $submission ? $submission->choice : '') === 'spirit' ? ' selected' : '' }}>Spirit</option>
                                <option value="dream"{{ old('framechoice', $submission ? $submission->choice : '') === 'dream' ? ' selected' : '' }}>Dream</option>
                                <option value="dedication"{{ old('framechoice', $submission ? $submission->choice : '') === 'dedication' ? ' selected' : '' }}>Dedication</option>
                                <option value="career"{{ old('framechoice', $submission ? $submission->choice : '') === 'career' ? ' selected' : '' }}>Career</option>
                                <option value="success"{{ old('framechoice', $submission ? $submission->choice : '') === 'success' ? ' selected' : '' }}>Success</option>
                            </select>
                            <span>Matters</span>
                        </div>
                    </div>

                    <div class="checkbox">
                        <input type="checkbox" name="agreement" id="agreement">
                        <label for="agreement">I agree to the <a href="{{ route('contest') }}#terms" target="_blank">terms and conditions</a></label>
                    </div>
                </form>
                <form class="step-content step-2">
                    <input type="hidden" name="step" value="2">
                    <h2 class="step-title">Step 2: Tell where you’re from and what you'll study</h2>

                    <div class="errors hidden"></div>

                    <div class="form-field">
                        <label for="">Country</label>
                        <select name="country">
                            <option value="">Select</option>
                            @foreach ($countries as $key => $name)
                                <option value="{{ $key }}"{{ old('country', $submission ? $submission->country : '') === $key ? ' selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-field">
                        <label for="">Hometown</label>
                        <input type="text" name="hometown" value="{{ old('hometown', $submission ? $submission->hometown : '') }}">
                    </div>

                    <div class="form-field">
                        <label for="">Lakehead Student ID</label>
                        <input type="text" name="student_id" value="{{ old('student_id', $submission ? $submission->student_id : '') }}">
                    </div>

                    <div class="form-field">
                        <label for="study">Level of Study</label>
                        <select name="study" id="study" class="study-level">
                            <option value="">Select</option>
                            <option value="undergraduate"{{ old('study', $submission ? $submission->level_of_study : '') === 'undergraduate' ? ' selected' : '' }}>Undergraduate (Bachelor)</option>
                            <option value="graduate"{{ old('study', $submission ? $submission->level_of_study : '') === 'graduate' ? ' selected' : '' }}>Graduate (Masters/PhD)</option>
                        </select>
                    </div>

                    <div class="form-field program-list program-undergraduate {{ old('study', $submission ? $submission->level_of_study : '') === 'undergraduate' ? '' : 'hidden' }}">
                        <label for="">Program</label>
                        <select name="program_undergrad">
                            <option value="">Select</option>
                            @foreach ($undergrads as $program)
                                <option{{ old('program_undergrad', $submission ? $submission->program : '') === $program ? ' selected' : '' }}>{{ $program }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-field program-list program-graduate {{ old('study', $submission ? $submission->level_of_study : '') === 'graduate' ? '' : 'hidden' }}">
                        <label for="">Program</label>
                        <select name="program_grad">
                            <option value="">Select</option>
                            @foreach ($grads as $program)
                                <option{{ old('program_grad', $submission ? $submission->program : '') === $program ? ' selected' : '' }}>{{ $program }}</option>
                            @endforeach
                        </select>
                    </div>
                </form>
                <form class="step-content step-3">
                    <input type="hidden" name="step" value="3">
                    <input type="hidden" name="photo" value="{{ old('photo', $submission ? $submission->originalfile : '') }}">
                    <h2 class="step-title">Step 3: Add your photo to the Incoming Class of 2022!</h2>
                    <p class="sub-title">Pick a spot in your community that means something to you and take a picture of yourself there. Or find an existing photo of yourself in a place that’s special to you.</p>

                    <div class="errors hidden"></div>

                    <h3>Upload your photo</h3>
                    <div class="upload-box" data-url="{{ url('') }}" data-uploads="{{ url('uploads') }}">
                        <p class="instructions"><span class="btn small outline">Click here to upload a photo.</span><br><br><small>Your photo must be a jpg/jpeg or png format.<br>Minimum size 1080 x 1350 pixels</small></p>
                    </div>

                    <div class="checkbox">
                        <input type="checkbox" name="original" id="original">
                        <label for="original">I confirm that the photo that I have submitted was either taken by me or I have the right to use and allow others to use it.</label>
                    </div>

                    <h3>Where was this photo taken?</h3>
                    <textarea name="location" class="story-input">{{ old('location', $submission ? $submission->location : '') }}</textarea>

                    <h3>What is special about this location?</h3>
                    <textarea name="location_story" class="story-input">{{ old('location_story', $submission ? $submission->location_details : '') }}</textarea>
                </form>
                <form class="step-content step-4">
                    <input type="hidden" name="step" value="4">
                    <input type="hidden" name="posx" value="{{ old('posx', $submission ? $submission->posx : '') }}">
                    <input type="hidden" name="posy" value="{{ old('posy', $submission ? $submission->posy : '') }}">
                    <input type="hidden" name="poswidth" value="{{ old('poswidth', $submission ? $submission->poswidth : '') }}">
                    <input type="hidden" name="posheight" value="{{ old('posheight', $submission ? $submission->posheight : '') }}">

                    <h2 class="step-title">Step 4: Preview Your Photo</h2>

                    <div class="errors hidden"></div>

                    <div class="frame-selection">
                        <div class="selection">
                            <h3>Which frame would you like?</h3>
                            <div class="frames">
                                @foreach ($frames as $frame)
                                    <label for="frame{{ $loop->index }}" class="{{ old('frame', $submission ? $submission->frame : '') === $frame ? 'active' : '' }}">
                                        <input type="radio" name="frame" id="frame{{ $loop->index }}" value="{{ $frame }}"{{ old('frame', $submission ? $submission->frame : '') === $frame ? 'checked' : '' }}>
                                        <img src="{{ asset('img/frames/thumbs/' . $frame . '.jpg') }}" alt="">
                                    </label>
                                @endforeach

                                <label for="frame-choice" class="{{ old('frame', $submission ? $submission->frame : '') === 'choice' ? 'active' : '' }}">
                                    <input type="radio" name="frame" id="frame-choice" value="choice"{{ old('frame', $submission ? $submission->frame : '') === 'choice' ? 'checked' : '' }}>
                                    <img src="{{ asset('img/frames/thumbs/my-' . old('framechoice', $submission ? $submission->choice : 'choice') . '-matters.jpg') }}" id="frame-choice-img" alt="" data-url="{{ asset('img/frames/thumbs/') }}">
                                </label>
                            </div>

                            <div class="crop-box bg-white">
                                <div class="crop-utility">
                                    <img src="{{ old('photo', $submission ? $submission->originalfile : '') }}" alt="">
                                </div>
                                <div class="text-center mt-6">
                                    <button type="button" class="btn btn-outline end-adjusting">
                                        <em class="far fa-times"></em>
                                        <span>Close</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="poster-holder">
                            <div class="poster-preview">
                                <div class="poster-image">
                                    <img src="{{ old('photo', $submission ? $submission->originalfile : '') }}">
                                </div>
                                <div class="poster-frame">
                                    @if (old('frame', $submission ? $submission->frame : '') !== '')
                                        <img src="{{ url('/img/frames/' . old('frame', $submission ? $submission->frame() : '') . '.png') }}">
                                    @endif
                                </div>
                            </div>

                            <div class="text-center mt-6">
                                <button type="button" class="btn btn-outline adjust-photo">
                                    <em class="far fa-crop"></em>
                                    <span>Adjust Photo</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <p class="sub-title text-center"><strong>That's it! Just click below, and we'll enter you into draws for our 10 amazing prizes!</strong></p>
                </form>

                <div class="text-center">
                    <a href="{{ url('/') }}" class="btn outline prev-step mx-2">
                        <em class="far fa-long-arrow-left"></em>
                        <span>Previous</span>
                    </a>
                    <button type="button" class="btn btn-primary next-step mx-2">
                        <span>Next</span>
                        <em class="far fa-long-arrow-right"></em>
                    </button>
                    <button type="button" class="btn btn-primary hidden final-step mx-2">
                        <span>Submit</span>
                        <em class="far fa-check"></em>
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
