<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <style type="text/css">
        body { padding:0; margin:0; font-family: Frutiger, "Frutiger Linotype", Univers, Calibri, "Gill Sans", "Gill Sans MT", "Myriad Pro", Myriad, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height: 1; background: #efefef; }
        img { border: 0; }
        a { text-decoration: none; }
    </style>
</head>
<body style="background: #00427a;">
    <div style="background: #00427a; color: #ffffff; padding: 10px;">
        <table border="0" cellpadding="10" cellspacing="0" style="max-width: 600px; margin: 0 auto;" width="100%">
            <tr>
                <td width="50%" align="right">
                    <img src="{{ asset('img/logo.png') }}" alt="Lakehead Univerity" style="max-width: 100%;">
                </td>
                <td width="50%" align="left">
                    <h1 style="font-size: 32px; color: #FFC20E;">NEW POST</h1>
                </td>
            </tr>
        </table>
    </div>
    <div style="background: #00427a; color: #ffffff; padding: 30px 20px; line-height: 1.4em;">
        <div style="max-width: 600px; margin: 0 auto; text-align: center;">
            <p>There are new posts awaiting your approval.</p>
            <img src="{{ asset('storage/' . $submission->finalphoto) }}" width="400" alt="" style="margin: 8px 0;" />
            <p><a href="{{ url('dashboard/posters') }}" style="display: inline-block; padding: 8px 16px; border-radius: 60px; background: #FFC20E; color: black;">VIEW SUBMISSIONS</a></p>
        </div>
    </div>
</body>
</html>
