<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <style type="text/css">
        body { padding:0; margin:0; font-family: Frutiger, "Frutiger Linotype", Univers, Calibri, "Gill Sans", "Gill Sans MT", "Myriad Pro", Myriad, "DejaVu Sans Condensed", "Liberation Sans", "Nimbus Sans L", Tahoma, Geneva, "Helvetica Neue", Helvetica, Arial, sans-serif; line-height: 1; background: #efefef; }
        img { border: 0; }
        a { text-decoration: none; }
    </style>
</head>
<body style="background: #00427a;">
    <div style="background: #00427a; color: #ffffff; padding: 10px;">
        <table border="0" cellpadding="10" cellspacing="0" style="max-width: 600px; margin: 0 auto;" width="100%">
            <tr>
                <td width="50%" align="right">
                    <img src="{{ asset('img/logo.png') }}" alt="Lakehead Univerity" style="max-width: 100%;">
                </td>
                <td width="50%" align="left">
                    <h1 style="font-size: 32px; color: #FFC20E;">THANK YOU</h1>
                </td>
            </tr>
        </table>
    </div>
    <div style="background: #00427a; color: #ffffff; padding: 30px 20px; line-height: 1.4em;">
        <div style="max-width: 600px; margin: 0 auto;">
            <p>Hello {{ $submission->first }},</p>
            <p>Thank you for participating in the Your Choice Matters campaign! We're excited to welcome you to the Incoming Class of 2022. Your submission has been entered into our draw for 10 amazing prizes!</p>
            <p>Please find attached your submitted photo. Feel free to share it on your social media channels and tag @LakeheadInternational</p>
            <p>Follow our social media channels for winner announcements starting mid-May with the grand prize announcement on June 1, 2022.</p>
            <p>Find us: @LakeheadInternational on <a href="https://www.instagram.com/lakeheadinternational/" style="color: #ffffff; text-decoration: underline;">Instagram</a>, <a href="https://www.facebook.com/LakeheadInternational/" style="color: #ffffff; text-decoration: underline;">Facebook</a> and <a href="https://www.youtube.com/lakeheadinternational" style="color: #ffffff; text-decoration: underline;">YouTube</a></p>
            <p>We look forward to seeing you on campus!</p>
            <p>~ The Lakehead International Team</p>
        </div>
    </div>
</body>
</html>
