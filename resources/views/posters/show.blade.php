@extends('layouts.public')

@section('content')
    @include('layouts.header', [
        'headings' => [
            'Education',
            'Journey',
            'Voice',
            'Dream',
            'Choice',
        ]
    ])

    <div class="share-screen text-center pt-normal pb-normal">
        <div class="container">
            <h1 class="text-blue uppercase title-large">THANK YOU FOR SHARING YOUR LAKEHEAD PRIDE!</h1>
            <p class="text-large">You have been entered into 10 amazing prize draws. Follow our social media channels and check your Lakehead email for winner announcements starting in mid-May with the grand prize announcement on June 1, 2022.</p>

            <ul class="social-icons text-blue">
                <li><a href="https://www.instagram.com/lakeheadinternational/" target="_blank"><em class="fab fa-instagram"></em></a></li>
                <li><a href="https://www.facebook.com/LakeheadInternational/" target="_blank"><em class="fab fa-facebook"></em></a></li>
                <li><a href="https://www.youtube.com/lakeheadinternational" target="_blank"><em class="fab fa-youtube"></em></a></li>
            </ul>

            <img src="{{ asset('storage/' . $submission->finalphoto) }}" alt="" class="poster">

            <div class="mt-6">
                <a href="{{ asset('storage/' . $submission->finalphoto) }}" class="button" target="_blank">
                    <span>Download &amp; Share Your Image</span>
                    <em class="far fa-comments"></em>
                </a>
            </div>
        </div>
    </div>
@endsection
