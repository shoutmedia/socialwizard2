@extends('rapture::layouts.dashboard')

@section('title', __('posters.plural'))

@section('content')
    @heading
        @lang('posters.plural')
    @endheading

    <div class="section-header">
        <ul class="tab-headings">
            <li class="tab active"><a href="#complete">Complete</a></li>
            <li class="tab"><a href="#incomplete">Incomplete</a></li>
        </ul>
    </div>

    @statuses

    <div class="container">
        <div class="tab-panel active" id="complete">
            <div class="poster-list">
                @foreach ($completed as $poster)
                    <div class="poster-item">
                        <a href="{{ route('dashboard.posters.edit', $poster) }}" class="edit-poster">
                            <em class="far fa-pen"></em>
                        </a>
                        <a href="{{ url('submission/' . $poster->ref) }}" target="_blank">
                            <img src="{{ asset('storage/' . $poster->finalphoto) }}" alt="">
                        </a>
                        <div class="poster-details">
                            Submitted By
                            <br>
                            <strong><a href="mailto:{{ $poster->email }}">{{ $poster->first }} {{ $poster->last }} <em class="far fa-paper-plane"></em></a></strong>
                            <br><br>
                            Email
                            <br>
                            <strong>{{ $poster->email }}</strong>
                            <br><br>
                            Program
                            <br>
                            <strong>{{ $poster->program }}</strong>
                        </div>
                        <div class="poster-status">
                            <button type="button" class="btn accept-poster {{ is_null($poster->approved_by) ? '' : 'hidden' }}" data-url="{{ route('dashboard.poster.approve', $poster) }}">Promote</button>
                            <button type="button" class="btn outline remove-poster {{ is_null($poster->approved_by) ? 'hidden' : '' }}" data-url="{{ route('dashboard.poster.remove', $poster) }}">Demote</button>

                            <button type="button" class="btn danger decline-poster" data-url="{{ route('dashboard.poster.deny', $poster) }}">Delete</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="tab-panel" id="incomplete">
            <div class="poster-list">
                @foreach ($incomplete as $poster)
                    <div class="poster-item">
                        <a href="{{ route('dashboard.posters.edit', $poster) }}" class="edit-poster">
                            <em class="far fa-pen"></em>
                        </a>
                        <a href="{{ url('submission/' . $poster->ref) }}" target="_blank">
                            <img src="{{ asset('storage/' . $poster->finalphoto) }}" alt="">
                        </a>
                        <div class="poster-details">
                            Submitted By
                            <br>
                            <strong><a href="mailto:{{ $poster->email }}">{{ $poster->first }} {{ $poster->last }} <em class="far fa-paper-plane"></em></a></strong>
                            <br><br>
                            Email
                            <br>
                            <strong>{{ $poster->email }}</strong>
                            <br><br>
                            Program
                            <br>
                            <strong>{{ $poster->program }}</strong>
                        </div>
                        <div class="poster-status">
                            <button type="button" class="btn danger decline-poster" data-url="{{ route('dashboard.poster.deny', $poster->id) }}">Delete</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endpush
