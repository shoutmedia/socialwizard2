@extends('rapture::layouts.dashboard')

@section('title', __('posters.plural'))

@section('content')
    @heading
        @lang('posters.plural')
    @endheading

    @statuses

    <form action="{{ route('dashboard.posters.update', $submission) }}" method="post" data-url="{{ url('') }}">
        @method('PUT')
        @csrf

        <input type="hidden" name="photo" value="{{ !is_null($submission->photo) ? $submission->photo : '' }}">

        <div class="hidden-fields"></div>

        <div class="container">
            <div class="box">
                <div class="poster-edit-screen">
                    <div class="form">
                        <h3>Step 1</h3>
                        <div class="form-field">
                            <label for="firstname">First Name</label>
                            <input type="text" name="firstname" id="firstname" value="{{ old('first', $submission->first) }}">
                        </div>
                        <div class="form-field">
                            <label for="lastname">Last Name</label>
                            <input type="text" name="lastname" id="lastname" value="{{ old('last', $submission->last) }}">
                        </div>
                        <div class="form-field">
                            <label for="email">Email</label>
                            <input type="email" name="emailaddress" id="email" value="{{ old('email', $submission->email) }}">
                        </div>
                        <div class="form-field choice">
                            <label for="story">Mindset</label>
                            <select name="framechoice">
                                <option value="choice"{{ old('framechoice', $submission ? $submission->choice : '') === 'choice' ? ' selected' : '' }}>Choice</option>
                                <option value="dream"{{ old('framechoice', $submission ? $submission->choice : '') === 'dream' ? ' selected' : '' }}>Dream</option>
                                <option value="education"{{ old('framechoice', $submission ? $submission->choice : '') === 'education' ? ' selected' : '' }}>Education</option>
                                <option value="journey"{{ old('framechoice', $submission ? $submission->choice : '') === 'journey' ? ' selected' : '' }}>Journey</option>
                                <option value="voice"{{ old('framechoice', $submission ? $submission->choice : '') === 'voice' ? ' selected' : '' }}>Voice</option>
                                <option value="curiosity"{{ old('framechoice', $submission ? $submission->choice : '') === 'curiosity' ? ' selected' : '' }}>Curiosity</option>
                                <option value="experience"{{ old('framechoice', $submission ? $submission->choice : '') === 'experience' ? ' selected' : '' }}>Experience</option>
                                <option value="home"{{ old('framechoice', $submission ? $submission->choice : '') === 'home' ? ' selected' : '' }}>Home</option>
                                <option value="family"{{ old('framechoice', $submission ? $submission->choice : '') === 'family' ? ' selected' : '' }}>Family</option>
                                <option value="culture"{{ old('framechoice', $submission ? $submission->choice : '') === 'culture' ? ' selected' : '' }}>Culture</option>
                                <option value="community"{{ old('framechoice', $submission ? $submission->choice : '') === 'community' ? ' selected' : '' }}>Community</option>
                                <option value="wellness"{{ old('framechoice', $submission ? $submission->choice : '') === 'wellness' ? ' selected' : '' }}>Wellness</option>
                                <option value="food"{{ old('framechoice', $submission ? $submission->choice : '') === 'food' ? ' selected' : '' }}>Food</option>
                                <option value="comfort"{{ old('framechoice', $submission ? $submission->choice : '') === 'comfort' ? ' selected' : '' }}>Comfort</option>
                                <option value="spirit"{{ old('framechoice', $submission ? $submission->choice : '') === 'spirit' ? ' selected' : '' }}>Spirit</option>
                                <option value="dream"{{ old('framechoice', $submission ? $submission->choice : '') === 'dream' ? ' selected' : '' }}>Dream</option>
                                <option value="dedication"{{ old('framechoice', $submission ? $submission->choice : '') === 'dedication' ? ' selected' : '' }}>Dedication</option>
                                <option value="career"{{ old('framechoice', $submission ? $submission->choice : '') === 'career' ? ' selected' : '' }}>Career</option>
                                <option value="success"{{ old('framechoice', $submission ? $submission->choice : '') === 'success' ? ' selected' : '' }}>Success</option>
                            </select>
                        </div>
                    </div>
                    <div class="form">
                        <h3>Step 2</h3>
                        <div class="form-field">
                            <label for="">Country</label>
                            <select name="country">
                                @foreach ($countries as $key => $name)
                                    <option value="{{ $key }}"{{ old('country', $submission->country) === $key ? ' selected' : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-field">
                            <label for="">Hometown</label>
                            <input type="text" name="hometown" value="{{ old('hometown', $submission->hometown) }}">
                        </div>
                        <div class="form-field">
                            <label for="">Lakehead Student ID</label>
                            <input type="text" name="student_id" value="{{ old('student_id', $submission->student_id) }}">
                        </div>
                        <div class="form-field">
                            <label for="study">Level of Study</label>
                            <select name="study" id="study" class="study-level">
                                <option value="undergraduate"{{ old('study', $submission->level_of_study) === 'undergraduate' ? ' selected' : '' }}>Undergraduate (Bachelor)</option>
                                <option value="graduate"{{ old('study', $submission->level_of_study) === 'graduate' ? ' selected' : '' }}>Graduate (Masters/PhD)</option>
                            </select>
                        </div>
                        <div class="form-field">
                            <label for="">Program</label>
                            <select name="program">
                                <optgroup label="Undergraduate">
                                    @foreach ($undergrads as $program)
                                        <option{{ old('program_undergrad', $submission->program) === $program ? ' selected' : '' }}>{{ $program }}</option>
                                    @endforeach
                                </optgroup>
                                <optgroup label="Graduate">
                                    @foreach ($grads as $program)
                                        <option{{ old('program_grad', $submission->program) === $program ? ' selected' : '' }}>{{ $program }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="form">
                        <h3>Step 3</h3>
                        <div class="form-field">
                            <label for="story">Location</label>
                            <textarea name="location">{{ old('location', $submission->location) }}</textarea>
                        </div>
                        <div class="form-field">
                            <label for="story">Location Story</label>
                            <textarea name="location_story">{{ old('location_story', $submission->location_details) }}</textarea>
                        </div>

                        <input type="hidden" name="posx" value="{{ old('posx', $submission->posx) }}">
                        <input type="hidden" name="posy" value="{{ old('posy', $submission->posy) }}">
                        <input type="hidden" name="poswidth" value="{{ old('poswidth', $submission->poswidth) }}">
                        <input type="hidden" name="posheight" value="{{ old('posheight', $submission->posheight) }}">
                    </div>
                </div>

                <button type="submit" class="btn">Save Changes</button>
            </div>

            <div class="box">
                <h3 style="margin-top: 0;">Frame Selection</h3>
                <div class="frames">
                    @foreach ($frames as $frame)
                        <label for="frame{{ $loop->index }}" class="{{ old('frame', $submission ? $submission->frame : '') === $frame ? 'active' : '' }}">
                            <input type="radio" name="frame" id="frame{{ $loop->index }}" value="{{ $frame }}"{{ old('frame', $submission ? $submission->frame : '') === $frame ? 'checked' : '' }}>
                            <img src="{{ asset('img/frames/thumbs/' . $frame . '.jpg') }}" alt="">
                        </label>
                    @endforeach

                    <label for="frame-choice" class="{{ old('frame', $submission ? $submission->frame : '') === 'choice' ? 'active' : '' }}">
                        <input type="radio" name="frame" id="frame-choice" value="choice"{{ old('frame', $submission ? $submission->frame : '') === 'choice' ? 'checked' : '' }}>
                        <img src="{{ asset('img/frames/thumbs/my-' . old('framechoice', $submission ? $submission->choice : 'choice') . '-matters.jpg') }}" id="frame-choice-img" alt="" data-url="{{ asset('img/frames/thumbs/') }}">
                    </label>
                </div>

                <button type="submit" class="btn">Save Changes</button>
            </div>

            <div class="box">
                <div class="crop-box">
                    <div class="crop-holder">
                        <div class="crop-utility">
                            <img src="{{ old('photo', $submission->originalfile) }}" alt="">
                        </div>
                    </div>

                    <div class="preview">
                        <div class="poster-preview">
                            <div class="poster-image">
                                <img src="{{ old('photo', $submission->originalfile) }}">
                            </div>
                            <div class="poster-frame">
                                <img src="{{ url('/img/frames/' . old('frame', $submission->frame()) . '.png') }}">
                            </div>
                        </div>

                        <div style="text-align: center; margin-top: 1.5rem;">
                            <a href="{{ $submission->originalfile }}" class="btn" target="_blank">Raw Image</a>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn">Save Changes</button>
            </div>
        </div>
    </form>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/cropper.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/dashboard.css?20190515') }}">
@endpush

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/cropper.min.js') }}"></script>
    <script src="{{ asset('js/dashboard.js') }}"></script>
@endpush
