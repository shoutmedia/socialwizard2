@extends('layouts.public')

@section('content')
    @include('layouts.header', [
        'headings' => [
            'Education',
            'Journey',
            'Voice',
            'Dream',
            'Choice',
        ]
    ])

    @if (count($posters) > 0)
    <div class="pt-normal pb-normal">
        <div class="container">
            <div class="mb-6">
                <h2 class="title-xlarge uppercase text-blue">Introducing the <span class="text-yellow">Incoming Class of 2022</span></h2>
                <p class="text-md">As an admitted applicant, you are an integral member of our new student body. Share your Lakehead pride, join the incoming class, and you'll have a chance to win some incredible prizes, including free ﬁrst-year residence.</p>
            </div>
            <div class="flex flex-wrap -mx-4">
                @foreach ($posters as $poster)
                <div class="w-1/4 px-4 py-4">
                    <img src="{{ asset('storage/' . $poster->finalphoto) }}" alt="" class="shadow-md">
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif
@endsection
