@extends('layouts.public')

@section('content')
    @include('layouts.header', [
        'headings' => [
            'Education',
            'Journey',
            'Voice',
            'Dream',
            'Choice',
        ]
    ])

    <div class="pt-normal pb-normal text-black text-md">
        <div class="container">
            <div class="columns items-center">
                <div class="column">
                    <h2 class="title-large text-blue uppercase"><span class="text-yellow">Studying abroad</span><br>starts with a dream...</h2>
                    <p>Perhaps your community’s or your family's dream for you, but most importantly, your dream. Every significant achievement in your life begins by imagining a future where you’re realizing your greatest potential. Lakehead is privileged to be part of your plans, and we'd love to dream along with you.</p>
                </div>
                <div class="column">
                    <img src="{{ asset('img/your-future-matters.jpg') }}" alt="" class="w-full rounded-md">
                </div>
            </div>
        </div>
    </div>

    <div class="bg-blue text-white pt-small pb-small text-md">
        <div class="container">
            <div class="columns items-center">
                <div class="column">
                    <h2 class="title-xlarge uppercase">Be the Face of<br><span class="text-yellow">Lakehead!</span></h2>
                    <p>
                        <a href="{{ route('contest') }}" class="button btn-yellow">
                            <span>Full Prizes &amp; Details</span>
                            <em class="far fa-trophy"></em>
                        </a>
                    </p>
                </div>
                <div class="column">
                    <p>Share your Lakehead pride and you'll have a chance to win some incredible prizes, including free first-year residence plus meal plan! See all 10 prizes below.</p>
                    <p>Follow our social media channels for winner announcements starting in mid-May with the grand prize announcement on June 1, 2022.</p>
                    <ol class="text-yellow font-bold">
                        <li>1st Year Residence + Meal Plan ($14,000 CAD)</li>
                        <li>Flight Voucher ($2500 CAD)</li>
                        <li>Winter Fun Pack ($1000 CAD)</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    @if (count($posters) > 0)
    <div class="featured-stories text-white pt-normal pb-normal bg-center bg-cover" style="background-image: url({{ asset('img/featuredbg.jpg') }})">
        <div class="container">
            <div class="mb-6">
                <h2 class="title-xlarge uppercase">Introducing the <span class="text-yellow">Incoming Class of 2022</span></h2>
                <p class="text-md">As an admitted applicant, you are an integral member of our new student body. Share your Lakehead pride, join the incoming class, and you'll have a chance to win some incredible prizes, including free ﬁrst-year residence.</p>
            </div>
            <div class="slides -mx-4">
                @foreach ($posters as $poster)
                <div class="slide px-4">
                    <img src="{{ asset('storage/' . $poster->finalphoto) }}" alt="" class="shadow-md">
                </div>
                @endforeach
            </div>
        </div>
    </div>
    @endif

    <div class="pt-small pb-small text-center text-black">
        <div class="container">
            <h2 class="title-large uppercase">Submit your <span class="text-blue">photo</span></h2>
            <p class="text-md">We'll enter you into draws for 10 amazing prizes!</p>
            <p><a href="{{ url('begin') }}" class="button">Participate now to win <em class="far fa-long-arrow-right"></em></a></p>
        </div>
    </div>

    <div class="pt-normal pb-normal bg-blue text-white">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <blockquote>
                        <p>All of my professors are really good when it comes to relationships with students. The Chair of my program helped me out a lot when I was transitioning to the university here. She also helped me start volunteering in the community, which evolved into working here at Lakehead. I will always be in debt to her for that.</p>
                        <cite>
                            <p><strong>Francisco from Mexico</strong><br>Outdoor Recreation, Parks & Tourism</p>
                        </cite>
                        <a href="{{ route('education') }}" class="button btn-yellow btn-small icon-before">
                            <em class="far fa-graduation-cap"></em>
                            <span>Your Education Matters</span>
                        </a>
                    </blockquote>
                </div>
                <div class="column">
                    <blockquote>
                        <p>The thing I admire about Canada is the people; they are very welcoming. I expected to miss my home festivals, but not only did I get to see my culture celebrated so beautifully here, I also loved seeing people from all across the city join together to celebrate it.</p>
                        <cite>
                            <p><strong>Alankrit from India</strong><br>Computer Science</p>
                        </cite>
                        <a href="{{ route('home') }}" class="button btn-yellow btn-small icon-before">
                            <em class="far fa-home"></em>
                            <span>Your Home Matters</span>
                        </a>
                    </blockquote>
                </div>
            </div>
            <div class="columns">
                <div class="column">
                    <blockquote>
                        <p>Lakehead has helped me develop myself as an independent person. It has helped me discover myself. Here I have the ability to do things I didn't even think I could do. A lot of positive things that I see in myself now were not in me two years ago. How did I get these things? They were all through Lakehead.</p>
                        <cite>
                            <p><strong>El-Amin from Nigeria</strong><br>Media, Film & Communications</p>
                        </cite>
                        <a href="{{ route('experience') }}" class="button btn-yellow btn-small icon-before">
                            <em class="far fa-plane-departure"></em>
                            <span>Your Experience Matters</span>
                        </a>
                    </blockquote>
                </div>
                <div class="column">
                    <blockquote>
                        <p>I did a co-op position as a technician at a medical clinic. I loved this position and I was offered a full time job when I finish school. I plan to work there and apply for permanent residency. The employer is even willing to work with me on that and support me all the way through that process.</p>
                        <cite>
                            <p><strong>Celine from Indonesia</strong><br>Kinesiology</p>
                        </cite>
                        <a href="{{ route('future') }}" class="button btn-yellow btn-small icon-before">
                            <em class="far fa-watch"></em>
                            <span>Your Future Matters</span>
                        </a>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>

    <div class="pb-normal pt-normal">
        <div class="container flex flex-wrap items-center justify-center">
            <a href="{{ route('contest') }}#sponsors"><img src="{{ asset('img/logo1.jpg') }}" alt="" class="my-2 mx-4"></a>
            <a href="{{ route('contest') }}#sponsors"><img src="{{ asset('img/logo2.jpg') }}" alt="" class="my-2 mx-4"></a>
            <a href="{{ route('contest') }}#sponsors"><img src="{{ asset('img/logo3.jpg') }}" alt="" class="my-2 mx-4"></a>
            <a href="{{ route('contest') }}#sponsors"><img src="{{ asset('img/logo4.jpg') }}" alt="" class="my-2 mx-4"></a>
            <a href="{{ route('contest') }}#sponsors"><img src="{{ asset('img/logo5.jpg') }}" alt="" class="my-2 mx-4"></a>
        </div>
    </div>
@endsection
