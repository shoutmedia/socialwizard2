@extends('layouts.public')

@section('content')
    @include('layouts.header', [
        'headings' => [
            'Education',
            'Journey',
            'Voice',
            'Dream',
            'Choice',
        ]
    ])
@endsection
