import axios from 'axios';
import serialize from 'form-serialize';
import FileUpload from './fileupload';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

const nextStep = document.querySelector('.next-step');
const prevStep = document.querySelector('.prev-step');
const finalStep = document.querySelector('.final-step');
const rawData = document.querySelector('.raw-data');
const agreement = document.getElementById('agreement');
const original = document.getElementById('original');
const studyLevel = document.querySelector('.study-level');
const frameChoices = document.querySelectorAll('.frames label');
const uploadBox = document.querySelector('.upload-box');
const posx = document.querySelector('input[name="posx"]');
const posy = document.querySelector('input[name="posy"]');
const poswidth = document.querySelector('input[name="poswidth"]');
const posheight = document.querySelector('input[name="posheight"]');
const adjustPhoto = document.querySelector('.adjust-photo');
const choice = document.querySelector('select[name="framechoice"]');

let currentStep = 1;
let cropper = null;

function processStep(form, callback = null, increment = true) {
    const icon = nextStep.querySelector('.far');
    const icon2 = finalStep.querySelector('.far');
    const errorholder = form.querySelector('.errors');

    icon.classList.remove('fa-long-arrow-right');
    icon.classList.add('fa-spinner');
    icon.classList.add('fa-spin');

    icon2.classList.remove('fa-check');
    icon2.classList.add('fa-spinner');
    icon2.classList.add('fa-spin');

    errorholder.classList.add('hidden');
    errorholder.innerHTML = '';

    axios.post(rawData.getAttribute('action'), serialize(form)).then((response) => {
        icon.classList.remove('fa-spin');
        icon.classList.remove('fa-spinner');
        icon.classList.add('fa-long-arrow-right');

        icon2.classList.remove('fa-spin');
        icon2.classList.remove('fa-spinner');
        icon2.classList.add('fa-check');

        if (increment) {
            incrementSteps();
        }

        if (callback !== null) {
            callback(response);
        }
    }).catch((error) => {
        if (error.response) {
            errorholder.classList.remove('hidden');

            const errors = Object.entries(error.response.data.errors)
                .map(entry => `<li>${entry[1]}</li>`)
                .join('');

            errorholder.innerHTML = `<ul>${errors}</ul>`;
        }

        icon.classList.remove('fa-spin');
        icon.classList.remove('fa-spinner');
        icon.classList.add('fa-long-arrow-right');

        icon2.classList.remove('fa-spin');
        icon2.classList.remove('fa-spinner');
        icon2.classList.add('fa-check');
    });
}

function incrementSteps() {
    const activeStepBody = document.querySelector('.user-steps .step-content.active');
    const nextStepBody = document.querySelector(`.user-steps .step-${currentStep + 1}`);

    if (nextStepBody) {
        nextStepBody.classList.add('active');
    }

    if (activeStepBody) {
        activeStepBody.classList.remove('active');
    }

    currentStep += 1;
}

function decrementSteps() {
    const activeStepBody = document.querySelector('.user-steps .step-content.active');
    const nextStepBody = document.querySelector(`.user-steps .step-${currentStep - 1}`);

    finalStep.classList.add('hidden');
    nextStep.classList.remove('hidden');

    if (nextStepBody) {
        nextStepBody.classList.add('active');
    }

    if (activeStepBody) {
        activeStepBody.classList.remove('active');
    }

    currentStep -= 1;
}

function enableCropper() {
    const image = document.querySelector('.crop-utility img');

    if (cropper !== null) {
        cropper.destroy();
    }

    cropper = new Cropper(image, {
        aspectRatio: 540 / 675,
        dragMode: 'none',
        zoomable: false,
        preview: '.poster-image',
        autoCropArea: 1,
        data: {
            x: parseInt(posx.value, 10),
            y: parseInt(posy.value, 10),
            width: parseInt(poswidth.value, 10),
            height: parseInt(posheight.value, 10),
        },
        crop(event) {
            posx.value = event.detail.x;
            posy.value = event.detail.y;
            poswidth.value = event.detail.width;
            posheight.value = event.detail.height;
        },
    });
}

if (choice) {
    choice.addEventListener('change', () => {
        const thumbnail = document.getElementById('frame-choice-img');
        const frameChoice = document.querySelector('label[for="frame-choice"].active');

        thumbnail.src = `${thumbnail.getAttribute('data-url')}/my-${choice.value}-matters.jpg`;

        if (frameChoice) {
            frameChoice.click();
        }
    });
}

Array.from(frameChoices).forEach((frame) => {
    const posterFrame = document.querySelector('.poster-frame');

    frame.addEventListener('click', () => {
        const previousSelection = document.querySelector('.frames label.active');
        const selected = frame.querySelector('input').value;

        if (previousSelection) {
            previousSelection.classList.remove('active');
        }

        frame.classList.add('active');

        if (selected === 'choice') {
            posterFrame.innerHTML = `<img src="${uploadBox.getAttribute('data-url')}/img/frames/my-${choice.value}-matters.png">`;
        } else {
            posterFrame.innerHTML = `<img src="${uploadBox.getAttribute('data-url')}/img/frames/${selected}.png">`;
        }
    });
});

if (studyLevel) {
    studyLevel.addEventListener('change', () => {
        const programLists = document.querySelectorAll('.program-list');

        Array.from(programLists).forEach((list) => {
            list.classList.add('hidden');
        });

        const activeList = document.querySelector(`.program-${studyLevel.value}`);

        activeList.classList.remove('hidden');
    });
}

if (nextStep) {
    nextStep.addEventListener('click', () => {
        if (currentStep === 1) {
            const step1 = document.querySelector('.step-1');
            processStep(step1);
        }

        if (currentStep === 2) {
            const step2 = document.querySelector('.step-2');
            processStep(step2);
        }

        if (currentStep === 3) {
            const step3 = document.querySelector('.step-3');
            processStep(step3, () => {
                enableCropper();

                finalStep.classList.remove('hidden');
                nextStep.classList.add('hidden');
            });
        }
    });
}

if (prevStep) {
    prevStep.addEventListener('click', (e) => {
        if (currentStep <= 1) {
            return;
        }

        e.preventDefault();
        decrementSteps();
    });
}

if (finalStep) {
    finalStep.addEventListener('click', () => {
        const step4 = document.querySelector('.step-4');

        processStep(step4, (response) => {
            window.location.href = response.data.url;
        }, false);
    });
}

if (uploadBox) {
    const upload = new FileUpload({
        container: uploadBox,
        instructions: '.how-to',
        upload: uploadBox.getAttribute('data-uploads'),
        class: 'asset',
        multi: false,
        fieldKey: 'file',
        filters: {
            mime_types: [
                {
                    title: 'Files',
                    extensions: 'jpg,jpeg,png,gif',
                },
            ],
        },
    });

    upload.on('uploaded', (e) => {
        const posterImage = document.querySelector('.poster-image img');
        const cropImage = document.querySelector('.crop-utility img');
        const existingInput = document.querySelector('input[name="photo"]');

        posx.value = '';
        posy.value = '';
        poswidth.value = '';
        posheight.value = '';

        if (existingInput) {
            existingInput.value = e.detail.path;
        }

        posterImage.setAttribute('src', `${uploadBox.getAttribute('data-url')}${e.detail.path}`);
        cropImage.setAttribute('src', `${uploadBox.getAttribute('data-url')}${e.detail.path}`);
    });
}

if (adjustPhoto) {
    const cropUtility = document.querySelector('.crop-box');

    adjustPhoto.addEventListener('click', (e) => {
        e.preventDefault();

        const selection = document.querySelector('.selection');

        if (cropUtility.classList.contains('active')) {
            cropUtility.classList.remove('active');
            selection.removeAttribute('style');
            return;
        }

        cropUtility.classList.add('active');
        selection.style.minHeight = `${cropUtility.clientHeight + 10}px`;
    });

    const endAdjusting = document.querySelector('.end-adjusting');

    endAdjusting.addEventListener('click', (e) => {
        e.preventDefault();
        cropUtility.classList.remove('active');

        const selection = document.querySelector('.selection');
        selection.removeAttribute('style');
    });
}

$(document).ready(function(){
    $('.slides').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });

    $('.slideshow').slick({
        infinite: true,
        arrows: false,
        autoplay: true,
    });
});
