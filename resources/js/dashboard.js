import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;

const tabHeadings = document.querySelectorAll('.tab-headings .tab a');
const approveButtons = document.querySelectorAll('.accept-poster');
const demoteButtons = document.querySelectorAll('.remove-poster');
const denyButtons = document.querySelectorAll('.decline-poster');

Array.from(tabHeadings).forEach((anchor) => {
    anchor.addEventListener('click', (e) => {
        e.preventDefault();

        const tab = anchor.closest('.tab');
        const activeTab = document.querySelector('.tab.active');
        const panel = document.querySelector(anchor.getAttribute('href'));
        const activePanel = document.querySelector('.tab-panel.active');

        if (tab.classList.contains('active')) {
            return;
        }

        tab.classList.add('active');
        activeTab.classList.remove('active');
        panel.classList.add('active');
        activePanel.classList.remove('active');
    });
});

Array.from(approveButtons).forEach((button) => {
    button.addEventListener('click', () => {
        const buttons = button.closest('.poster-status');
        const removeBtn = buttons.querySelector('.remove-poster');

        removeBtn.classList.remove('hidden');
        button.classList.add('hidden');

        axios.post(button.getAttribute('data-url'), {
            _method: 'PUT',
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
    });
});

Array.from(demoteButtons).forEach((button) => {
    button.addEventListener('click', () => {
        const buttons = button.closest('.poster-status');
        const addBtn = buttons.querySelector('.accept-poster');

        addBtn.classList.remove('hidden');
        button.classList.add('hidden');

        axios.post(button.getAttribute('data-url'), {
            _method: 'PUT',
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
    });
});

Array.from(denyButtons).forEach((button) => {
    button.addEventListener('click', () => {
        const parent = button.closest('.poster-item');
        parent.remove();

        axios.post(button.getAttribute('data-url'), {
            _method: 'DELETE',
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        });
    });
});

/**
 * Editor
 */
const image = document.querySelector('.crop-utility img');
const choice = document.querySelector('select[name="framechoice"]');

if (image) {
    const posx = document.querySelector('input[name="posx"]');
    const posy = document.querySelector('input[name="posy"]');
    const poswidth = document.querySelector('input[name="poswidth"]');
    const posheight = document.querySelector('input[name="posheight"]');

    const cropper = new Cropper(image, {
        aspectRatio: 540 / 675,
        dragMode: 'none',
        zoomable: false,
        preview: '.poster-image',
        autoCropArea: 1,
        data: {
            x: parseInt(posx.value, 10),
            y: parseInt(posy.value, 10),
            width: parseInt(poswidth.value, 10),
            height: parseInt(posheight.value, 10),
        },
        crop(event) {
            posx.value = event.detail.x;
            posy.value = event.detail.y;
            poswidth.value = event.detail.width;
            posheight.value = event.detail.height;
        },
    });
}

const frameChoices = document.querySelectorAll('.frames label');

Array.from(frameChoices).forEach((frame) => {
    const posterFrame = document.querySelector('.poster-frame');
    const form = document.querySelector('form[data-url]');

    frame.addEventListener('click', () => {
        const previousSelection = document.querySelector('.frames label.active');
        const selected = frame.querySelector('input').value;

        if (previousSelection) {
            previousSelection.classList.remove('active');
        }

        frame.classList.add('active');

        if (selected === 'choice') {
            posterFrame.innerHTML = `<img src="${form.getAttribute('data-url')}/img/frames/my-${choice.value}-matters.png">`;
        } else {
            posterFrame.innerHTML = `<img src="${form.getAttribute('data-url')}/img/frames/${selected}.png">`;
        }
    });
});

if (choice) {
    choice.addEventListener('change', () => {
        const thumbnail = document.getElementById('frame-choice-img');
        const frameChoice = document.querySelector('label[for="frame-choice"].active');

        thumbnail.src = `${thumbnail.getAttribute('data-url')}/my-${choice.value}-matters.jpg`;

        if (frameChoice) {
            frameChoice.click();
        }
    });
}
