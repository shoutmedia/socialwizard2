<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadsController extends Controller
{
    public function store(Request $request)
    {
        $fileName = implode('.', [
            Str::before($request->file('file')->hashName(), '.'),
            $request->file('file')->getClientOriginalExtension()
        ]);

        $path = $request->file('file')->storeAs('public', $fileName);

        return Storage::url($path);
    }
}
