<?php

namespace App\Http\Controllers;

use App\Submission;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(Request $request)
    {
        $posters = Submission::whereNotNull('approved_by')->limit(6)->orderByDesc('created_at')->get();

        return view('pages.index', compact('posters'));
    }

    public function gallery(Request $request)
    {
        $posters = Submission::whereNotNull('approved_by')->orderByDesc('created_at')->get();

        return view('pages.gallery', compact('posters'));
    }

    public function education(Request $request)
    {
        return view('pages.education');
    }

    public function home(Request $request)
    {
        return view('pages.home');
    }

    public function experience(Request $request)
    {
        return view('pages.experience');
    }

    public function future(Request $request)
    {
        return view('pages.future');
    }

    public function contest(Request $request)
    {
        return view('pages.contest');
    }
}
