<?php

namespace App\Http\Controllers;

use App\Helpers\FrameBuilder;
use App\Submission;
use Illuminate\Http\Request;

class TestBuildController extends Controller
{
    public function index()
    {
        $submission = Submission::first();

        $poster = new FrameBuilder(public_path($submission->originalfile));
        $poster->crop($submission->poswidth, $submission->posheight, $submission->posx, $submission->posy);
        $poster->resize(1080, 1350);

        $poster->newLayer(0, 0);
        $poster->addImage(public_path('img/frames/' . $submission->frame . '.png'));

        return $poster->display();
    }
}
