<?php

namespace App\Http\Controllers\Posters;

use App\Http\Controllers\Controller;
use App\Submission;
use Illuminate\Http\Request;

class RemoveFromFeed extends Controller
{
    public function __invoke(Request $request, Submission $submission)
    {
        $submission->approved_by = null;
        $submission->save();
    }
}
