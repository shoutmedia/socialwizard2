<?php

namespace App\Http\Controllers\Posters;

use App\Http\Controllers\Controller;
use App\Submission;
use Illuminate\Http\Request;

class DeleteEntry extends Controller
{
    public function __invoke(Submission $submission)
    {
        $submission->delete();
    }
}
