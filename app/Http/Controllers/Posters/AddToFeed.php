<?php

namespace App\Http\Controllers\Posters;

use App\Http\Controllers\Controller;
use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddToFeed extends Controller
{
    public function __invoke(Request $request, Submission $submission)
    {
        $submission->approved_by = Auth::id();
        $submission->save();
    }
}
