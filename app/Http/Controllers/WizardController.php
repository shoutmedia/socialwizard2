<?php

namespace App\Http\Controllers;

use App\Helpers\CountryList;
use App\Helpers\FrameStyle1;
use App\Helpers\ProgramList;
use App\Mail\PendingPoster;
use App\Mail\PosterApproved;
use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WizardController extends Controller
{
    public function index(Request $request, $ref = '')
    {
        $submission = Submission::where('ref', $this->refKey($ref))->first();
        $countries = CountryList::getList();
        $grads = ProgramList::getGraduate();
        $undergrads = ProgramList::getUnderGraduates();

        $frames = [
            'incoming-class-of-2022',
            'my-dream',
            'thunderwolf-4-life',
            'thunderwolves-for-life',
            'pennant-and-flag',
        ];

        return view('wizard.index', [
            'submission' => $submission,
            'countries' => $countries,
            'grads' => $grads,
            'undergrads' => $undergrads,
            'frames' => $frames,
        ]);
    }

    public function store(Request $request)
    {
        $method = 'step' . $request->input('step');

        return $this->$method($request);
    }

    private function step1(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'emailaddress' => 'required|email:filter',
            'framechoice' => 'required',
            'agreement' => 'required',
        ]);

        Submission::updateOrInsert([
            'ref' => $this->refKey(),
        ], [
            'first' => $request->input('firstname'),
            'last' => $request->input('lastname'),
            'email' => $request->input('emailaddress'),
            'choice' => $request->input('framechoice'),
            'accepted' => $request->filled('agreement'),
        ]);

        return response()->json([
            'url' => url('submission/' . $this->refKey()),
        ]);
    }

    private function step2(Request $request)
    {
        $rules = [
            'country' => 'required',
            'hometown' => 'required',
            'student_id' => 'required',
            'study' => 'required',
        ];

        if ($request->input('study') === 'graduate') {
            $rules['program_grad'] = 'required';
        } else {
            $rules['program_undergrad'] = 'required';
        }

        $this->validate($request, $rules);

        Submission::updateOrInsert([
            'ref' => $this->refKey(),
        ], [
            'country' => $request->input('country'),
            'hometown' => $request->input('hometown'),
            'student_id' => $request->input('student_id'),
            'level_of_study' => $request->input('study'),
            'program' => $request->input('study') === 'graduate' ? $request->input('program_grad') : $request->input('program_undergrad'),
        ]);

        return response()->json([
            'url' => url('submission/' . $this->refKey()),
        ]);
    }

    private function step3(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required',
            'original' => 'required',
            'location' => 'required',
            'location_story' => 'required',
        ]);

        Submission::updateOrInsert([
            'ref' => $this->refKey(),
        ], [
            'originalfile' => $request->input('photo'),
            'confirmation' => $request->filled('original'),
            'location_details' => $request->input('location_story'),
            'location' => $request->input('location'),
        ]);

        return response()->json([
            'url' => url('submission/' . $this->refKey()),
        ]);
    }

    private function step4(Request $request)
    {
        $this->validate($request, [
            'posx' =>  'required',
            'posy' =>  'required',
            'poswidth' =>  'required',
            'posheight' =>  'required',
            'frame' =>  'required',
        ]);

        $ref = $this->refKey();

        Submission::updateOrInsert([
            'ref' => $ref,
        ], [
            'posx' => round($request->input('posx')),
            'posy' => round($request->input('posy')),
            'poswidth' => round($request->input('poswidth')),
            'posheight' => round($request->input('posheight')),
            'frame' => $request->input('frame'),
        ]);

        $submission = Submission::where('ref', $ref)->first();

        $filename = new FrameStyle1($submission);

        $submission->finalphoto = $filename->filename;
        $submission->is_complete = true;
        $submission->save();

        Mail::to($submission->email)->send(new PosterApproved($submission));
        Mail::to('socialmedia.intl@lakeheadu.ca')->send(new PendingPoster($submission));

        session()->forget('ref_key');

        return response()->json([
            'url' => url('submission/' . $ref),
        ]);
    }

    private function refKey($ref = '')
    {
        if (!empty($ref)) {
            session()->put('ref_key', $ref);
            return $ref;
        }

        $key = session()->get('ref_key');

        if (is_null($key)) {
            $key = md5(time());
            session()->put('ref_key', $key);
        }

        return $key;
    }
}
