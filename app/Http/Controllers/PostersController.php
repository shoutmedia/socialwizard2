<?php

namespace App\Http\Controllers;

use App\Helpers\CountryList;
use App\Helpers\FrameStyle1;
use App\Helpers\ProgramList;
use App\Submission;
use Illuminate\Http\Request;

class PostersController extends Controller
{
    public function index()
    {
        $completed = Submission::where('is_complete', true)->get();
        $incomplete = Submission::where('is_complete', false)->get();

        return view('posters.index', compact('completed', 'incomplete'));
    }

    public function edit(Submission $poster)
    {
        $countries = CountryList::getList();
        $grads = ProgramList::getGraduate();
        $undergrads = ProgramList::getUnderGraduates();

        $frames = [
            'incoming-class-of-2022',
            'my-dream',
            'thunderwolf-4-life',
            'thunderwolves-for-life',
            'pennant-and-flag',
        ];

        return view('posters.edit', [
            'submission' => $poster,
            'countries' => $countries,
            'grads' => $grads,
            'undergrads' => $undergrads,
            'frames' => $frames,
        ]);
    }

    public function update(Request $request, Submission $poster)
    {
        $poster->update([
            'first' => $request->input('firstname'),
            'last' => $request->input('lastname'),
            'email' => $request->input('emailaddress'),
            'choice' => $request->input('framechoice'),
            'country' => $request->input('country'),
            'hometown' => $request->input('hometown'),
            'student_id' => $request->input('student_id'),
            'level_of_study' => $request->input('study'),
            'program' => $request->input('program'),
            'location_details' => $request->input('location_story'),
            'location' => $request->input('location'),
            'posx' => round($request->input('posx')),
            'posy' => round($request->input('posy')),
            'poswidth' => round($request->input('poswidth')),
            'posheight' => round($request->input('posheight')),
            'frame' => $request->input('frame'),
        ]);

        $submission = Submission::find($poster->id);

        $filename = new FrameStyle1($submission);

        $submission->finalphoto = $filename->filename;
        $submission->is_complete = true;
        $submission->save();

        return redirect()
            ->route('dashboard.posters.index');
    }
}
