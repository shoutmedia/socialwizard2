<?php

namespace App\Http\Controllers;

use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PosterPreviewsController extends Controller
{
    public function show($ref)
    {
        $submission = Submission::where('ref', $ref)->first();

        return view('posters.show', compact('submission'));
    }
}
