<?php

namespace App\Helpers;

use Intervention\Image\ImageManagerStatic as Image;

class SocialBuilder
{
    public $fonts = [];
    public $img;
    public $top = 32;
    public $margin = 32;
    public $color = '#ffffff';
    public $previewWidth = 500;
    public $previewHeight = 500;
    public $width = 1000;
    public $height = 1000;

    public function __construct($background)
    {
        $this->img = Image::make($background);
        $this->img->fit($this->width, $this->height);
    }

    public function setEncode($key)
    {
        $this->img->encode($key);
    }

    public function addFont($key, $options = [])
    {
        $this->fonts[$key] = $options;
    }

    public function addLines($lines, $font, $overrides = [])
    {
        $currentFont = array_merge($this->fonts[$font], $overrides);

        $fontSize = $currentFont['size'];
        $color = array_key_exists('color', $currentFont) ? $currentFont['color'] : $this->color;
        $segments = collect($lines);

        foreach ($segments as $key => $line) {
            $yPosition = $this->top + $currentFont['margin'] + ($fontSize * $currentFont['line'] * $key);
            $topPosition = round($yPosition);
            $xPosition = round($this->width / 2);

            $this->img->text($line, $xPosition, $topPosition, function ($font) use ($fontSize, $currentFont, $color) {
                $font->file($currentFont['file']);
                $font->size($fontSize);
                $font->color($color);
                $font->valign('top');
                $font->align('center');
            });
        }

        $this->top = $yPosition + $fontSize;
    }

    public function addImage($src, $width = null, $height = null, $options = [])
    {
        $args = array_merge([
            'margin' => 0,
            'position' => 'top-left',
            'increment' => true,
        ], $options);

        $watermark = Image::make($src);

        if (!is_null($width) || !is_null($height)) {
            $watermarkWidth = round($width);
            $watermarkHeight = round($height);

            $watermark->resize($watermarkWidth, $watermarkHeight, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $topPosition = round(($this->top + $args['margin']));
        $xPosition = round($this->margin);
        $newHeight = round($watermark->height());

        $this->img->insert($watermark, $args['position'], $xPosition, $topPosition);

        if ($args['increment']) {
            $this->top += $newHeight + $args['margin'];
        }
    }

    public function addCircleImage($src, $width = null, $height = null, $options = [])
    {
        $args = array_merge([
            'margin' => 0,
            'position' => 'top-left',
            'increment' => true,
        ], $options);

        $watermark = Image::make($src);
        $circleMask = Image::canvas($width, $height);

        $circleMask->circle($width, $width/2, $height/2, function ($draw) {
            $draw->background('#fff');
        });

        if (!is_null($width) || !is_null($height)) {
            $watermarkWidth = round($width);
            $watermarkHeight = round($height);

            if ($watermark->getHeight() > $watermark->getWidth()) {
                $watermarkHeight = null;
            } elseif ($watermark->getHeight() < $watermark->getWidth()) {
                $watermarkWidth = null;
            }

            $watermark->resize($watermarkWidth, $watermarkHeight, function ($constraint) {
                $constraint->aspectRatio();
            })->crop($width, $height);
        }

        $topPosition = round(($this->top + $args['margin']));
        $xPosition = round($this->margin);
        $newHeight = round($watermark->height());

        $watermark->mask($circleMask, false);

        $this->img->insert($watermark, $args['position'], $xPosition, $topPosition);

        if ($args['increment']) {
            $this->top += $newHeight + $args['margin'];
        }
    }

    public function newLayer($top = 32, $margin = 32)
    {
        $this->top = $top;
        $this->margin = $margin;
    }

    public function special()
    {
        $fontFile = public_path('fonts/TradeGothicLTPro-BdCn20.ttf');
        $fontSize = 40;
        $bottomMargin = 50;
        $rightMargin = 80;

        $y = $this->height - $bottomMargin - $fontSize;
        $x = $this->width - $rightMargin;

        $this->img->text('UNCONVENTIONAL.', $x, $y, function ($font) use ($fontSize, $fontFile) {
            $font->file($fontFile);
            $font->size($fontSize);
            $font->color('#FFFFFF');
            $font->valign('top');
            $font->align('right');
        });

        $x -= 270;

        $this->img->text('EXCEPTIONAL.', $x, $y, function ($font) use ($fontSize, $fontFile) {
            $font->file($fontFile);
            $font->size($fontSize);
            $font->color('#004B88');
            $font->valign('top');
            $font->align('right');
        });
    }

    public function logo()
    {
        $watermark = Image::make(public_path('img/poster-logo.png'));
        $watermark->resize(242, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $this->img->insert($watermark, 'bottom-left', 80, 50);
    }

    public function display()
    {
        return $this->img->response();
    }

    public function save($format = 'jpg')
    {
        $filename = date('YmdHis') . random_int(100, 999) . '.' . $format;

        $this->img->save(storage_path('app/public/' . $filename), 100);

        return $filename;
    }
}
