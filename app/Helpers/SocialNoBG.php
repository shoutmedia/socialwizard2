<?php

namespace App\Helpers;

use App\Helpers\SocialBuilder;
use Illuminate\Http\Request;

class SocialNoBG
{
    public $filename = '';

    public function __construct(Request $request)
    {
        $poster = new SocialBuilder(public_path('img/overlay.png'));
        $poster->setEncode('png');

        // Set up
        $poster->addFont('title', [
            'size' => 48,
            'file' => public_path('fonts/TradeGothicLTPro-Bd2.ttf'),
            'line' => 1,
            'margin' => 16,
            'color' => '#5E368A',
        ]);

        $poster->addFont('headline', [
            'size' => 38,
            'file' => public_path('fonts/TradeGothicLTPro-Bd2.ttf'),
            'line' => 1.2,
            'margin' => 16,
            'color' => '#FFC325',
        ]);

        $poster->addFont('paragraph', [
            'size' => 34,
            'file' => public_path('fonts/TradeGothicLTStd-Light.ttf'),
            'line' => 1.3,
            'margin' => 16,
            'color' => '#231F20',
        ]);

        // Compiling
        $poster->newLayer(0, 0);

        $poster->addCircleImage(public_path($request->input('photo')), 182, 182, [
            'margin' => 28,
            'position' => 'top',
        ]);

        $poster->addImage(public_path('img/quotes.png'), 81, 47, [
            'margin' => 46,
            'position' => 'top',
        ]);

        $poster->addLines([
            strtoupper($request->input('first') . ' ' . $request->input('last')),
        ], 'title', [
            'margin' => 60,
        ]);

        if ($request->has('depart')) {
            $departs = collect($request->input('depart'))->map(function ($name) {
                return strtoupper($name);
            });

            $poster->addLines($departs, 'headline');
        }

        if ($request->has('line')) {
            $poster->addLines($request->input('line'), 'paragraph', [
                'margin' => 40,
            ]);
        }

        $this->filename = $poster->save('png');
    }
}
