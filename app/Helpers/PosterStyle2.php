<?php

namespace App\Helpers;

use App\Helpers\PosterBuilder;
use Illuminate\Http\Request;

class PosterStyle2
{
    public $filename = '';

    public function __construct(Request $request)
    {
        $poster = new PosterBuilder(public_path('img/nbg' . $request->input('background') . '.jpg'));

        $poster->addFont('title', [
            'size' => 44,
            'file' => public_path('fonts/TradeGothicLTBold.ttf'),
            'line' => 1,
            'margin' => 16,
            'color' => '#FFC20E',
        ]);

        $poster->addFont('headline', [
            'size' => 20,
            'file' => public_path('fonts/TradeGothicLTBold.ttf'),
            'line' => 1.2,
            'margin' => 16,
            'color' => '#000000',
        ]);

        $poster->addFont('paragraph', [
            'size' => 14,
            'file' => public_path('fonts/OpenSans-Regular.ttf'),
            'line' => 1.4,
            'margin' => 16,
            'color' => '#000000',
        ]);

        $poster->addFont('department', [
            'size' => 14,
            'file' => public_path('fonts/OpenSans-Regular.ttf'),
            'line' => 1,
            'margin' => 0,
            'color' => '#000000',
        ]);

        if ($request->has('headline')) {
            $poster->addLines($request->input('headline'), 'title', [
                'margin' => 28,
            ]);
        }

        $poster->newLayer();

        if ($request->has('line')) {
            $poster->addLines($request->input('line'), 'paragraph', [
                'margin' => 408,
            ]);
        }

        $poster->newLayer(408, 362);

        if ($request->filled('photo')) {
            $poster->addImage(public_path($request->input('photo')), 158, 150);
        }

        $poster->addLines([
            $request->input('first') . ' ' . $request->input('last'),
        ], 'headline');

        $poster->addLines([
            $request->input('depart'),
        ], 'department');

        $this->filename = $poster->save();
    }
}
