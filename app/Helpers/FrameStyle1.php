<?php

namespace App\Helpers;

use App\Helpers\FrameBuilder;
use App\Submission;

class FrameStyle1
{
    public $filename = '';

    public function __construct(Submission $submission)
    {
        $poster = new FrameBuilder(public_path($submission->originalfile));
        $poster->crop($submission->poswidth, $submission->posheight, $submission->posx, $submission->posy);
        $poster->resize(1080, 1350);

        $poster->newLayer(0, 0);
        $poster->addImage(public_path('img/frames/' . $submission->frame() . '.png'));

        $this->filename = $poster->save();
    }
}
