<?php

namespace App\Helpers;

use Intervention\Image\ImageManagerStatic as Image;

class PosterBuilder
{
    public $fonts = [];
    public $img;
    public $scale;
    public $top = 32;
    public $margin = 32;
    public $color = '#ffffff';
    public $previewWidth = 549;
    public $previewHeight = 710;
    public $width = 1024;
    public $height = 1325;

    public function __construct($background)
    {
        $this->scale = $this->width / $this->previewWidth;

        $this->img = Image::make($background);
        $this->img->fit($this->width, $this->height);
    }

    public function addFont($key, $options = [])
    {
        $this->fonts[$key] = $options;
    }

    public function addLines($lines, $font, $overrides = [])
    {
        $currentFont = array_merge($this->fonts[$font], $overrides);

        $fontSize = $currentFont['size'];
        $scaledFontSize = $fontSize * $this->scale;
        $color = array_key_exists('color', $currentFont) ? $currentFont['color'] : $this->color;
        $segments = collect($lines);

        foreach ($segments as $key => $line) {
            $yPosition = $this->top + $currentFont['margin'] + ($fontSize * $currentFont['line'] * $key);
            $topPosition = round($yPosition * $this->scale);
            $xPosition = round($this->margin * $this->scale);

            $this->img->text($line, $xPosition, $topPosition, function ($font) use ($scaledFontSize, $currentFont, $color) {
                $font->file($currentFont['file']);
                $font->size($scaledFontSize);
                $font->color($color);
                $font->valign('top');
            });
        }

        $this->top = $yPosition + $fontSize;
    }

    public function addImage($src, $width = null, $height = null, $options = [])
    {
        $args = array_merge([
            'margin' => 0,
            'position' => 'top-left',
            'increment' => true,
        ], $options);

        $watermark = Image::make($src);

        if (!is_null($width) || !is_null($height)) {
            $watermarkWidth = round($width * $this->scale);
            $watermarkHeight = round($height * $this->scale);

            $watermark->resize($watermarkWidth, $watermarkHeight, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $topPosition = round(($this->top + $args['margin']) * $this->scale);
        $xPosition = round($this->margin * $this->scale);
        $newHeight = round($watermark->height() / $this->scale);

        $this->img->insert($watermark, $args['position'], $xPosition, $topPosition);

        if ($args['increment']) {
            $this->top += $newHeight;
        }

        return $newHeight;
    }

    public function newLayer($top = 32, $margin = 32)
    {
        $this->top = $top;
        $this->margin = $margin;
    }

    public function special()
    {
        $currentFont = $this->fonts['headline'];

        $fontSize = $currentFont['size'];
        $scaledFontSize = $fontSize * $this->scale;

        $yPosition = $this->previewHeight - 120 - $fontSize;
        $topPosition = round($yPosition * $this->scale);
        $xPosition = round($this->margin * $this->scale);

        $this->img->text('EXCEPTIONAL.', $xPosition, $topPosition, function ($font) use ($scaledFontSize, $currentFont) {
            $font->file($currentFont['file']);
            $font->size($scaledFontSize);
            $font->color('#FFC20E');
            $font->valign('top');
        });

        $xPosition = round(($this->margin + 135) * $this->scale);

        $this->img->text('UNCONVENTIONAL.', $xPosition, $topPosition, function ($font) use ($scaledFontSize, $currentFont) {
            $font->file($currentFont['file']);
            $font->size($scaledFontSize);
            $font->color('#ffffff');
            $font->valign('top');
        });
    }

    public function logo()
    {
        $watermark = Image::make(public_path('img/poster-logo.png'));

        $topPosition = round(28 * $this->scale);
        $xPosition = round(28 * $this->scale);

        $this->img->insert($watermark, 'bottom-right', $xPosition, $topPosition);
    }

    public function display()
    {
        return $this->img->response();
    }

    public function save()
    {
        $filename = date('YmdHis') . random_int(100, 999) . '.jpg';

        $this->img->save(storage_path('app/public/' . $filename), 100);

        return $filename;
    }
}
