<?php

namespace App\Helpers;

use App\Helpers\PosterBuilder;
use Illuminate\Http\Request;

class PosterStyle1
{
    public $filename = '';

    public function __construct(Request $request)
    {
        $poster = new PosterBuilder(public_path('img/bg' . $request->input('background') . '.jpg'));

        $poster->addFont('title', [
            'size' => 40,
            'file' => public_path('fonts/TradeGothicLTBold.ttf'),
            'line' => 1,
            'margin' => 16,
            'color' => '#FFC20E',
        ]);

        $poster->addFont('headline', [
            'size' => 22,
            'file' => public_path('fonts/TradeGothicLTBold.ttf'),
            'line' => 1.2,
            'margin' => 16,
        ]);

        $poster->addFont('paragraph', [
            'size' => 16,
            'file' => public_path('fonts/OpenSans-Regular.ttf'),
            'line' => 1.4,
            'margin' => 16,
        ]);

        $poster->addFont('department', [
            'size' => 14,
            'file' => public_path('fonts/OpenSans-Regular.ttf'),
            'line' => 1,
            'margin' => 0,
        ]);

        $poster->addLines([
            'TOGETHER, WE MAKE A DIFFERENCE',
        ], 'headline', [
            'margin' => 0,
        ]);

        if ($request->has('headline')) {
            $poster->addLines($request->input('headline'), 'title');
        }

        $poster->addLines([
            'HOW DOES YOUR WORK MAKE AN IMPACT?',
        ], 'headline', [
            'margin' => 40,
        ]);

        if ($request->has('line')) {
            $poster->addLines($request->input('line'), 'paragraph');
        }

        $topMargin = 32;

        if ($request->filled('photo')) {
            $height = $poster->addImage(public_path($request->input('photo')), 194, 140, [
                'margin' => 32,
                'position' => 'top-right',
                'increment' => false,
            ]);

            // 32px margin + half the image height - (22px + 14px) / 2 = 18
            $topMargin = round(32 + ($height / 2) - 18);
        }

        $poster->addLines([
            $request->input('first') . ' ' . $request->input('last'),
        ], 'headline', [
            'margin' => $topMargin,
        ]);

        $poster->addLines([
            $request->input('depart'),
        ], 'department');

        $poster->special();
        $poster->logo();

        $this->filename = $poster->save();
    }
}
