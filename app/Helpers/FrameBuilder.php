<?php

namespace App\Helpers;

use Intervention\Image\ImageManagerStatic as Image;

class FrameBuilder
{
    public $img;
    public $top = 0;
    public $margin = 0;

    public function __construct($background)
    {
        $this->img = Image::make($background);
        $this->img->orientate();
    }

    public function crop($width, $height, $x = 0, $y = 0)
    {
        $this->img->crop($width, $height, $x, $y);
    }

    public function resize($width = 0, $height = 0)
    {
        $this->img->resize($width, $height);
    }

    public function setEncode($key)
    {
        $this->img->encode($key);
    }

    public function addImage($src, $width = null, $height = null, $options = [])
    {
        $args = array_merge([
            'margin' => 0,
            'position' => 'top-left',
            'increment' => true,
        ], $options);

        $watermark = Image::make($src);

        if (!is_null($width) || !is_null($height)) {
            $watermarkWidth = round($width);
            $watermarkHeight = round($height);

            $watermark->resize($watermarkWidth, $watermarkHeight, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        $topPosition = round(($this->top + $args['margin']));
        $xPosition = round($this->margin);
        $newHeight = round($watermark->height());

        $this->img->insert($watermark, $args['position'], $xPosition, $topPosition);

        if ($args['increment']) {
            $this->top += $newHeight + $args['margin'];
        }
    }

    public function newLayer($top = 0, $margin = 0)
    {
        $this->top = $top;
        $this->margin = $margin;
    }

    public function display()
    {
        return $this->img->response();
    }

    public function save($format = 'jpg')
    {
        $filename = date('YmdHis') . random_int(100, 999) . '.' . $format;

        $this->img->save(storage_path('app/public/' . $filename), 100);

        return $filename;
    }
}
