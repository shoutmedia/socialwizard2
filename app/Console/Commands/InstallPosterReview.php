<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Rapture\Core\Installer;

class InstallPosterReview extends Command
{
    use Installer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapture:posters:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add a poster review screen';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addMenu([
            'label' => 'posters.plural',
            'route' => 'dashboard.posters.index',
            'icon' => 'images',
            'namespaces' => ['dashboard/posters'],
        ]);

        $this->resourcePermissions('posters');
        $this->info('Posters installed!');
    }
}
