<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $guarded = [];

    public function frame()
    {
        if (empty($this->frame)) {
            return 'incoming-class-of-2022';
        }

        return $this->frame === 'choice' ? 'my-' . $this->choice . '-matters' : $this->frame;
    }
}
