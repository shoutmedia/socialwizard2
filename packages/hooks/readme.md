# Hooks

Add easy to use extensions to your existing package

## Installation

Install the package from composer.

```
composer install raptureapp/hooks
```

## Usage

Within your package

@listen
@filter

Hook::listen();
Hook::filter();

Available to other developers

Hook::event();
Hook::screen();
