<?php

namespace Rapture\Hooks\Contracts;

interface PrioritizedEvents
{
    public function dispatch($event, $payload);
    public function attach($events, $listener, $priority);
}
