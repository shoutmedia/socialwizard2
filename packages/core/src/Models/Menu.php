<?php

namespace Rapture\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $guarded = [];

    public function children()
    {
        return $this->hasMany('Rapture\Core\Models\Menu')->orderBy('position');
    }

    public function hasChildren()
    {
        return count($this->children) > 0;
    }

    public function setNamespacesAttribute($value)
    {
        $this->attributes['namespaces'] = json_encode($value);
    }

    public function getNamespacesAttribute()
    {
        return json_decode($this->attributes['namespaces']);
    }

    public function scopeTop($query)
    {
        return $query->whereNull('menu_id');
    }

    public function scopeDashboard($query)
    {
        return $query->where('placement', 'root');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function url()
    {
        return is_null($this->route) ? url($this->url) : route($this->route);
    }

    public function namespaces()
    {
        return collect($this->namespaces)
            ->map(function ($item, $key) {
                return $item . '*';
            })
            ->toArray();
    }
}
