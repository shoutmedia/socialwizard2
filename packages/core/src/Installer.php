<?php

namespace Rapture\Core;

use Illuminate\Support\Facades\Cache;
use Rapture\Core\Models\Menu;
use Rapture\Core\Models\MenuItem;
use Rapture\Keeper\Traits\Installer as PermissionInstaller;

trait Installer
{
    use PermissionInstaller;

    public function addMenu($options)
    {
        $fields = collect($options);

        Cache::forget('dashboard.menu');

        return Menu::firstOrCreate([
            'route' => $fields->get('route'),
            'menu_id' => $fields->get('menu_id', null),
        ], $fields->except('route')->toArray());
    }
}
