<?php

namespace Rapture\Core\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class InstallRaptureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapture:install {--seed : Add a default admin account}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install rapture along with all of it\'s packages';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Setting up database');
        $this->call('migrate');

        if ($this->option('seed')) {
            $this->info('Seeding user account');
            $this->call('db:seed', ['--class' => 'Rapture\Core\Seeder\UsersTableSeeder']);
        }

        $this->info('Installing packages');

        $process = new Process('php artisan list rapture --format=json');
        $process->setTimeout(null);
        $process->setWorkingDirectory(base_path())->run();

        $commandList = json_decode($process->getOutput());

        $commands = collect($commandList->commands)->pluck('name')->filter(function ($value) {
            return $value !== 'rapture:install' && substr($value, -8) === ':install';
        })->each(function ($item) {
            $this->call($item);
        });

        $this->call('vendor:publish', ['--tag' => 'rapture', '--force' => true]);

        $this->info('Rapture installed!');
    }
}
