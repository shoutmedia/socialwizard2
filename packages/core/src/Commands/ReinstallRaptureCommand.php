<?php

namespace Rapture\Core\Commands;

use Illuminate\Console\Command;

class ReinstallRaptureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapture:reinstall {--seed : Add back a default admin account}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reinstall rapture along with all of it\'s packages';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Resetting the database');
        $this->call('migrate:fresh');

        if ($this->option('seed')) {
            $this->call('rapture:install', ['--seed' => true]);
        } else {
            $this->call('rapture:install');
        }
    }
}
