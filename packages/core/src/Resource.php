<?php

namespace Rapture\Core;

class Resource
{
    protected $styles = [];
    protected $scripts = [];
    protected $components = [];

    public function register($name, $assets)
    {
        $assets = collect($assets);

        $this->styles = array_merge_recursive($this->styles, [$name => $assets->get('styles', [])]);
        $this->scripts = array_merge_recursive($this->scripts, [$name => $assets->get('scripts', [])]);
        $this->components = array_merge_recursive($this->components, [$name => $assets->get('components', [])]);
    }

    public function styles($name)
    {
        return collect($this->styles[$name] ?? []);
    }

    public function scripts($name)
    {
        return collect($this->scripts[$name] ?? []);
    }

    public function components($name)
    {
        return collect($this->components[$name] ?? []);
    }
}
