<?php

namespace Rapture\Core;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Rapture\Core\Commands\InstallRaptureCommand;
use Rapture\Core\Commands\ReinstallRaptureCommand;
use Rapture\Core\Facades\Resource as ResourceFacade;

class RaptureServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'rapture');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'rapture');

        $this->publishes([
            __DIR__ . '/../public' => public_path('rapture'),
        ], 'rapture');

        View::composer('rapture::layouts.dashboard', 'Rapture\Core\ViewComposers\DashboardComposer');

        Blade::include('rapture::partials.statuses', 'statuses');
        Blade::component('rapture::components.heading', 'heading');

        Blade::directive('resource', function ($expression) {
            $groups = collect(explode(',', $expression));

            $groups = $groups->map(function ($item) {
                return substr(trim($item), 1, -1);
            });

            $output = '';

            $groups->each(function ($group) use (&$output) {
                $components = ResourceFacade::components($group)->map(function ($item) {
                    return '<script src="' . asset($item) . '"></script>';
                });

                $scripts = ResourceFacade::scripts($group)->map(function ($item) {
                    return '<script src="' . asset($item) . '"></script>';
                });

                $styles = ResourceFacade::styles($group)->map(function ($item) {
                    return '<link rel="stylesheet" href="' . asset($item) . '">';
                });

                if ($components->count() > 0) {
                    $output .= "<?php \$__env->startPush('components'); ?>";
                    $output .= $components->implode('');
                    $output .= "<?php \$__env->stopPush(); ?>";
                }

                if ($scripts->count() > 0) {
                    $output .= "<?php \$__env->startPush('scripts'); ?>";
                    $output .= $scripts->implode('');
                    $output .= "<?php \$__env->stopPush(); ?>";
                }

                if ($styles->count() > 0) {
                    $output .= "<?php \$__env->startPush('styles'); ?>";
                    $output .= $styles->implode('');
                    $output .= "<?php \$__env->stopPush(); ?>";
                }
            });

            return $output;
        });

        Blade::directive('langAction', function ($expression) {
            return "<?php echo langAction({$expression}); ?>";
        });

        ResourceFacade::register('datepicker', [
            'styles' => ['rapture/css/pikaday.css'],
            'components' => ['rapture/js/datepicker.js'],
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallRaptureCommand::class,
                ReinstallRaptureCommand::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('resource', function () {
            return new Resource();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['resource'];
    }
}
