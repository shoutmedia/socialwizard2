<?php

namespace Rapture\Core\ViewComposers;

use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use Rapture\Core\Models\Menu;

class DashboardComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menus = Cache::rememberForever('dashboard.menu', function () {
            return Menu::dashboard()
                ->top()
                ->active()
                ->orderBy('position')
                ->with('children')
                ->get();
        });

        $view->with('dashboard_menus', $menus);
    }
}
