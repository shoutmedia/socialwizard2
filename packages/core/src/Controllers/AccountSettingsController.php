<?php

namespace Rapture\Core\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountSettingsController extends Controller
{
    public function index()
    {
        return view('rapture::account.index');
    }

    public function update(Request $request)
    {
        $validation = [
            'name' => 'required|max:255',
        ];

        if ($request->filled('password')) {
            $validation['password'] = 'required|min:6|confirmed';
        }

        $validatedData = $this->validate($request, $validation);

        auth()->user()->name = $request->input('name');

        if ($request->filled('password')) {
            auth()->user()->password = bcrypt($request->input('password'));
        }

        auth()->user()->save();

        return redirect()
            ->route('dashboard.account.index')
            ->with('status', __('rapture::account.update_success'));
    }
}
