<?php

namespace Rapture\Core\Seeder;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $timestamp = Carbon::now();

        $timestamps = [
            'created_at' => $timestamp,
            'updated_at' => $timestamp,
        ];

        DB::table('users')->insert(array_merge([
            'name' => 'Shout Media',
            'email' => 'web@shout-media.ca',
            'password' => bcrypt('password'),
        ], $timestamps));
    }
}
