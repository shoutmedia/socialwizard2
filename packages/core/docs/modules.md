# Modules

## Installer

Create an artisan command with the `rapture:YourModule:install` naming convention. When `rapture:install` is executed anything following this convention will automatically be executed as well. You may use the `Rapture\Core\Installer` trait to access some helper methods.

* addMenu
* registerPermission

## Appearances

* Extend core layout with `@extends('rapture::layouts.dashboard')`
* Add success and error messages with `@statuses`
* Add section heading with `@heading`
