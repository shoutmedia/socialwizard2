@extends('rapture::layouts.dashboard')

@section('title', __('rapture::dashboard.title'))

@section('content')
    @heading
        @lang('rapture::dashboard.title')
    @endheading

    <div class="container dashboard">
        <div class="flex">
            @hook('dashboard.widgets')
        </div>
    </div>
@endsection
