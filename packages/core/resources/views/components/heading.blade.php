<div class="section-header">
    <div class="container">
        @isset($before)
            <div class="flex flex-start">{{ $before }}</div>
        @endisset

        <h2 class="title">{{ $slot }}</h2>

        @isset($after)
            <div class="flex flex-end">{{ $after }}</div>
        @endisset
    </div>
</div>
