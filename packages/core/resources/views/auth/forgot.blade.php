@extends('rapture::layouts.content')

@section('content')
    @statuses

    <div class="login">
        <h3>@lang('rapture::forgot.title')</h3>

        <div class="box">
            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="form-field{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">@lang('rapture::forgot.field_email')</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                </div>

                <div class="form-field">
                    <button type="submit" class="btn full">@lang('rapture::forgot.form_submit')</button>
                </div>
            </form>
        </div>
    </div>
@endsection
