@extends('rapture::layouts.content')

@section('content')
    @statuses

    <div class="login">
        <h3>@lang('rapture::login.title')</h3>

        <div class="box">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-field{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">@lang('rapture::login.field_email')</label>
                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                </div>

                <div class="form-field{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">@lang('rapture::login.field_password')</label>
                    <input id="password" type="password" name="password" required>
                </div>

                <div class="form-field">
                    <div class="form-checkbox">
                        <input type="checkbox" id="remember" name="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember">@lang('rapture::login.field_remember')</label>
                    </div>
                </div>

                <div class="form-field">
                    <button type="submit" class="btn full">@lang('rapture::login.form_submit')</button>
                </div>
            </form>
        </div>

        <p class="align-center">
            <a href="{{ route('password.request') }}">@lang('rapture::login.forgot_password')</a>
        </p>
    </div>
@endsection
