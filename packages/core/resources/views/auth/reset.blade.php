@extends('rapture::layouts.content')

@section('content')
    @statuses

    <div class="login">
        <h3>@lang('rapture::resetpassword.title')</h3>

        <div class="box">
            <form method="POST" action="{{ route('password.request') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-field{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">@lang('rapture::resetpassword.field_email')</label>
                    <input id="email" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-field{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">@lang('rapture::resetpassword.field_password')</label>
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-field{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm">@lang('rapture::resetpassword.field_password_conf')</label>
                    <input id="password-confirm" type="password" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-field">
                    <button type="submit" class="btn full">@lang('rapture::resetpassword.form_submit')</button>
                </div>
            </form>
        </div>
    </div>
@endsection
