@extends('rapture::layouts.dashboard')

@section('title', __('rapture::account.title'))

@section('content')
    @heading
        @lang('rapture::account.title')
    @endheading

    @statuses

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ route('dashboard.account.update') }}">
                @csrf

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="userName">@lang('rapture::field.name') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="text" name="name" id="userName" value="{{ old('name', auth()->user()->name) }}">
                            </div>

                            @hook('account.details')
                        </div>

                        <div class="box">
                            <div class="form-field">
                                <label for="userPass">@lang('rapture::field.password')</label>
                                <input type="password" name="password" id="userPass">
                            </div>
                            <div class="form-field">
                                <label for="userPassConf">@lang('rapture::field.password_conf')</label>
                                <input type="password" name="password_confirmation" id="userPassConf">
                            </div>
                        </div>

                        @hook('account.content')
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">@langAction('update', __('rapture::account.menu_label'))</button>

                        @hook('account.sidebar')
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
