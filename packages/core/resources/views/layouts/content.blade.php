<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Rapture') }}</title>

        <link rel="stylesheet" href="{{ asset('rapture/css/public.css') }}">

        @stack('styles')

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header class="header">
                <div class="container">
                    <a href="{{ route('dashboard') }}" class="logo">
                        {{ config('app.name', 'Rapture') }}
                    </a>
                </div>
            </header>

            @yield('content')
        </div>

        @stack('scripts')
    </body>
</html>
