<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@hasSection('title')
            @yield('title') -
        @endif{{ config('app.name', 'Rapture') }}</title>

        @resource('rapture')

        @prepend('styles')
            <link rel="stylesheet" href="{{ asset('rapture/css/app.css') }}">
        @endprepend

        @stack('styles')

        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <header class="header">
                <a href="{{ route('dashboard') }}" class="logo">
                    {{ config('app.name', 'Rapture') }}
                </a>
                <div class="container">
                    <div class="breadcrumbs"></div>
                    <nav class="nav-secondary">
                        <ul class="menu">
                            <li class="menu-item profile is-dropdown">
                                <a href="#"><span>{{ auth()->user()->name }}</span></a>
                                <ul class="sub-menu slider-hidden">
                                    <li class="menu-item">
                                        <a href="{{ route('dashboard.account.index') }}"><em class="far fa-cog" aria-hidden="true"></em> @lang('rapture::account.menu_label')</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><em class="far fa-power-off" aria-hidden="true"></em> @lang('rapture::dashboard.logout')</a>
                                    </li>
                                </ul>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>

            <nav class="nav-primary">
                <ul class="menu">
                    <li class="menu-item{{ request()->is('dashboard') ? ' active' : '' }}">
                        <a href="{{ url('dashboard') }}">
                            <em class="far fa-tachometer-alt fa-fw" aria-hidden="true"></em>
                            <span>@lang('rapture::dashboard.menu_label')</span>
                        </a>
                    </li>
                    @foreach ($dashboard_menus as $menu)
                    <li class="menu-item{{ request()->is(...$menu->namespaces()) ? ' active' : '' }}">
                        <a href="{{ $menu->url() }}">
                            <em class="far fa-{{ $menu->icon }} fa-fw" aria-hidden="true"></em>
                            <span>@lang($menu->label)</span>
                            @if ($menu->hasChildren())
                                <button type="button" class="expand-menu">
                                    <em class="fas fa-caret-right fa-fw{{ request()->is(...$menu->namespaces()) ? ' fa-rotate-90' : '' }}"></em>
                                </button>
                            @endif
                        </a>
                        @if ($menu->hasChildren())
                        <ul class="sub-menu{{ request()->is(...$menu->namespaces()) ? '' : ' slider-hidden' }}">
                            @foreach ($menu->children as $child)
                                <li class="menu-item{{ request()->is(...$child->namespaces()) ? ' active' : '' }}">
                                    <a href="{{ $child->url() }}">@lang($child->label)</a>
                                </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
                <span class="app-version">0.1.0-beta</span>
            </nav>

            <main class="content-main">
                @yield('content')
            </main>

            <portal-target name="dialogs"></portal-target>
        </div>

        @prepend('components')
            @if (config('app.debug'))
                <script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js"></script>
            @else
                <script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
            @endif
        @endprepend

        @push('components')
            <script src="{{ asset('rapture/js/app.js') }}"></script>
        @endpush

        @stack('components')
        @stack('scripts')
    </body>
</html>
