@if (count($errors) > 0)
    <div class="alert-errors">
        <div class="container">
            <p class="alert-label">@lang('rapture::field.fix_errors'):</p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>
                        <em class="far fa-caret-right"></em>
                        <span>{{ $error }}</span>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
