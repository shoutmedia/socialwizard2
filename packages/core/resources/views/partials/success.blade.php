@if (session('status'))
    <div class="alert-success">
        <div class="container">
            <ul>
                <li>
                    <em class="far fa-check"></em>
                    <span>{{ session('status') }}</span>
                </li>
            </ul>
        </div>
    </div>
@endif
