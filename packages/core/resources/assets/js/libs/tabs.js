export default class Tabs {
    constructor(container, options = {}) {
        this.options = Object.assign({
            container,
            heading: 'ul.headings',
            panel: 'div.panel',
            active: 'active',
        }, options);

        this.bindTabs();
    }
    bindTabs() {
        const headings = this.options.container.querySelectorAll(`${this.options.heading} li.tab a`);

        Array.from(headings).forEach((target) => {
            target.addEventListener('click', (e) => {
                e.preventDefault();

                const href = target.getAttribute('href');
                const activePanel = document.getElementById(href.substr(1));

                if (!target.classList.contains('active')) {
                    this.clearActivePanel();
                    this.clearActiveTab();

                    activePanel.classList.add(this.options.active);
                    target.parentNode.classList.add(this.options.active);
                }
            });
        });
    }
    clearActivePanel() {
        this.removeClass(this.options.panel, this.options.active);
    }
    clearActiveTab() {
        this.removeClass(`${this.options.heading} li.tab`, this.options.active);
    }
    removeClass(query, className) {
        const grouping = this.options.container.querySelectorAll(query);

        Array.from(grouping).forEach((target) => {
            target.classList.remove(className);
        });
    }
    selectPaneById(selector) {
        this.clearActivePanel();
        this.clearActiveTab();

        const activePanel = document.getElementById(selector.substr(1));
        const activeTab = this.options.container.querySelector(`${this.options.heading} li.tab a[href="${selector}"]`);

        activePanel.classList.add(this.options.active);
        activeTab.parentNode.classList.add(this.options.active);
    }
}
