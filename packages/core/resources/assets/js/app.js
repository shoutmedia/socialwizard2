/* global Vue */
import axios from 'axios';
import PortalVue from 'portal-vue';
import { slideUp, slideDown } from './libs/helpers';
import DialogBox from '../components/DialogBox.vue';
import DeleteButton from '../components/DeleteButton.vue';
import NewEntry from '../components/NewEntry.vue';
import Tooltip from './libs/tooltip';

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content;

Vue.use(PortalVue);

Vue.component('dialog-box', DialogBox);
Vue.component('delete-button', DeleteButton);
Vue.component('new-entry', NewEntry);

Vue.config.productionTip = false;

window.eventbus = new Vue();
window.rapture = new Vue().$mount('#wrapper');

const dropdown = document.querySelectorAll('.is-dropdown > a');

Array.from(dropdown).forEach((target) => {
    target.addEventListener('click', (event) => {
        event.preventDefault();

        const menu = target.closest('.menu-item').querySelector('.sub-menu');

        if (menu.classList.contains('slider-hidden')) {
            slideDown(menu, 200, 'ease-in-out');
            target.classList.add('opened');
        } else {
            slideUp(menu, 200, 'ease-in-out');
            target.classList.remove('opened');
        }
    });
});

const expandMenu = document.querySelectorAll('.expand-menu');

Array.from(expandMenu).forEach((target) => {
    target.addEventListener('click', (event) => {
        event.preventDefault();

        const menu = target.closest('.menu-item').querySelector('.sub-menu');

        if (menu.classList.contains('slider-hidden')) {
            slideDown(menu, 200, 'ease-in-out');
            target.querySelector('.fas').classList.add('fa-rotate-90');
        } else {
            slideUp(menu, 200, 'ease-in-out');
            target.querySelector('.fas').classList.remove('fa-rotate-90');
        }
    });
});

const tooltips = document.querySelectorAll('[data-tooltip]');

if (tooltips) {
    Array.from(tooltips).forEach((target) => {
        const tooltip = new Tooltip(target);
    });
}
