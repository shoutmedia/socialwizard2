/* global Vue */
import Datepicker from '../components/Datepicker.vue';

Vue.component('datepicker', Datepicker);
