<?php

return [
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'password_conf' => 'Confirm Password',
    'required' => 'Required',
    'fix_errors' => 'Please fix the following errors',
];
