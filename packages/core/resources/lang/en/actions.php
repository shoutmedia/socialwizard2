<?php

return [
    'create' => 'Create',
    'update' => 'Update',
    'edit' => 'Edit',
    'save' => 'Save Changes',
    'return' => 'Return to List',
    'delete' => 'Delete',
    'select_all' => 'Select all',
    'deselect_all' => 'Deselect all',
    'new_model' => 'New :Model',
    'create_model' => 'Create :Model',
    'edit_model' => 'Edit :Model',
    'update_model' => 'Update :Model',
    'delete_model' => 'Delete :Model',
    'return_model' => 'Return to :Model',
    'cancel' => 'Cancel',
];
