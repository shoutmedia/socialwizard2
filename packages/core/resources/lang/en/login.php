<?php

return [
    'title' => 'Login',
    'field_email' => 'E-Mail Address',
    'field_password' => 'Password',
    'field_remember' => 'Remember Me',
    'forgot_password' => 'Forgot Your Password?',
    'form_submit' => 'Login',
];
