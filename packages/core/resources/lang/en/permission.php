<?php

return [
    'index' => 'View',
    'create' => 'Create',
    'edit' => 'Edit',
    'destroy' => 'Delete',
];
