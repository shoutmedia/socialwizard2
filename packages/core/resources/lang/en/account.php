<?php

return [
    'title' => 'Account Settings',
    'menu_label' => 'Account',
    'update_success' => 'Your account was successfully updated!',
];
