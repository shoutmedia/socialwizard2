<?php

return [
    'title' => 'Reset Password',
    'field_email' => 'E-Mail Address',
    'form_submit' => 'Send Password Reset Link',
];
