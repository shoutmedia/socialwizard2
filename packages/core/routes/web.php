<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Core\Controllers')
    ->group(function () {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    });

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Core\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::get('account', 'AccountSettingsController@index')->name('account.index');
        Route::post('account', 'AccountSettingsController@update')->name('account.update');
    });

Route::middleware(['web'])
    ->namespace('Rapture\Core\Controllers')
    ->group(function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('login.post');
        Route::post('logout', 'LoginController@logout')->name('logout');

        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset.post');
    });
