# Rapture

Version: `0.1.x`

## Getting Started

* Add all the rapture packages you want to your composer.json including this package
* Run the installer `php artisan rapture:install`

### Seeding the database

You may add a default user account by hand by running `php artisan db:seed --class="Rapture\Core\Seeder\UsersTableSeeder"`. Alternately, you may add the `--seed` option to the base install to have the seeder run for you.

## Default user account

* Email: **web@shout-media.ca**
* Password: **password**
