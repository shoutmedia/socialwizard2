const path = require('path');

const config = {
    mode: 'production',
    entry: {
        app: './resources/assets/js/app',
        public: './resources/assets/js/public',
    },
    output: {
        path: path.resolve(__dirname, 'public/js'),
        filename: '[name].js',
        chunkFilename: '[id].chunk.js',
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: 'babel-loader',
        }],
    },
};

module.exports = config;
