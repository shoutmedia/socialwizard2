@extends('rapture::layouts.dashboard')

@section('title', langAction('new', __('users::package.singular')))

@section('content')
    @heading
        @langAction('new', __('users::package.singular'))

        @slot('after')
            <a href="{{ route('dashboard.users.index') }}" class="btn-pill"><em class="far fa-reply"></em> @lang('rapture::actions.return')</a>
        @endslot
    @endheading

    @statuses

    <div class="content-builder">
        <div class="container">
            <form method="post" action="{{ route('dashboard.users.store') }}">
                @csrf

                <div class="row">
                    <div class="content">
                        <div class="box">
                            <div class="form-field">
                                <label for="userName">@lang('rapture::field.name') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="text" name="name" id="userName" value="{{ old('name') }}">
                            </div>
                            <div class="form-field">
                                <label for="userEmail">@lang('rapture::field.email') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="email" name="email" id="userEmail" value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="box">
                            <div class="form-field">
                                <label for="userPass">@lang('rapture::field.password') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="password" name="password" id="userPass">
                            </div>
                            <div class="form-field">
                                <label for="userPassConf">@lang('rapture::field.password_conf') <span class="required">@lang('rapture::field.required')</span></label>
                                <input type="password" name="password_confirmation" id="userPassConf">
                            </div>
                        </div>

                        @hook('users.create.content')
                    </div>
                    <div class="sidebar">
                        <button type="submit" class="btn full">@langAction('create', __('users::package.singular'))</button>

                        @hook('users.create.sidebar')
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
