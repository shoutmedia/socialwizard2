@extends('rapture::layouts.dashboard')

@section('title', __('users::package.plural'))

@section('content')
    @heading
        @lang('users::package.plural')

        @slot('after')
            @can('users.create')
                <a href="{{ route('dashboard.users.create') }}" class="btn"><em class="far fa-user-plus"></em> @langAction('new', __('users::package.singular'))</a>
            @endcan
        @endslot
    @endheading

    @statuses

    <div class="container">
        <table class="table striped">
            <tr>
                <th>@lang('rapture::field.name')</th>
                <th>@lang('rapture::field.email')</th>
                <th class="action"></th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td class="action">
                        @can('users.edit')
                        <a href="{{ route('dashboard.users.edit', $user->id) }}"><em class="far fa-pencil"></em></a>
                        @endcan
                        @can('users.destroy')
                        <delete-button action="{{ route('dashboard.users.destroy', $user->id) }}"></delete-button>
                        @endcan
                    </td>
                </tr>
            @endforeach
        </table>

        {{ $users->links() }}
    </div>
@endsection
