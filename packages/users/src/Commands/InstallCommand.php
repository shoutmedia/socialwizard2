<?php

namespace Rapture\Users\Commands;

use Illuminate\Console\Command;
use Rapture\Core\Installer;

class InstallCommand extends Command
{
    use Installer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rapture:users:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install user management';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->addMenu([
            'label' => 'users::package.plural',
            'route' => 'dashboard.users.index',
            'icon' => 'users',
            'namespaces' => ['dashboard/users'],
        ]);

        $this->resourcePermissions('users');
        $this->info('Users installed!');
    }
}
