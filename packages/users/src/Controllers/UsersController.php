<?php

namespace Rapture\Users\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Rapture\Hooks\Facades\Hook;
use Rapture\Users\Events\UserCreated;
use Rapture\Users\Events\UserDeleted;
use Rapture\Users\Events\UserUpdated;
use Rapture\Users\Requests\StoreUser;
use Rapture\Users\Requests\UpdateUser;

class UsersController extends Controller
{
    /**
     * User Listing
     */
    public function index()
    {
        $users = User::paginate(16);

        return view('users::dashboard.index', compact('users'));
    }

    /**
     * Create new user form
     */
    public function create()
    {
        return view('users::dashboard.create');
    }

    /**
     * Create user
     */
    public function store(StoreUser $request)
    {
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);

        Hook::dispatch('user.stored', new UserCreated($user));

        return redirect()
            ->route('dashboard.users.index')
            ->with('status', langAlert('created', __('users::package.singular')));
    }

    /**
     * Edit User
     */
    public function edit(User $user)
    {
        return view('users::dashboard.edit', compact('user'));
    }

    /**
     * Update User
     */
    public function update(UpdateUser $request, User $user)
    {
        if ($request->filled('password')) {
            $user->password = bcrypt($request->input('password'));
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->save();

        Hook::dispatch('user.updated', new UserUpdated($user));

        return redirect()
            ->route('dashboard.users.index')
            ->with('status', langAlert('updated', __('users::package.singular')));
    }

    /**
     * Delete User
     */
    public function destroy(User $user)
    {
        $user->delete();

        Hook::dispatch('user.deleted', new UserDeleted($user));

        return redirect()
            ->route('dashboard.users.index')
            ->with('status', langAlert('deleted', __('users::package.singular')));
    }
}
