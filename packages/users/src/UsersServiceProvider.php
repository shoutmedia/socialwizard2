<?php

namespace Rapture\Users;

use Illuminate\Support\ServiceProvider;
use Rapture\Users\Commands\CreateCommand;
use Rapture\Users\Commands\InstallCommand;
use Rapture\Users\Commands\ListCommand;

class UsersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'users');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'users');

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                CreateCommand::class,
                ListCommand::class,
            ]);
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
