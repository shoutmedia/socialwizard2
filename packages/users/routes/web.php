<?php

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Users\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('users', 'UsersController')->except('show');
    });
