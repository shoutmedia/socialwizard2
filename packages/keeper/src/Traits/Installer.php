<?php

namespace Rapture\Keeper\Traits;

use Rapture\Keeper\Models\Permission;

trait Installer
{
    public function registerPermission($group, $action, $description)
    {
        $latest = Permission::where([
            'group' => $group,
        ])->orderBy('priority', 'desc')->first();

        return Permission::firstOrCreate([
            'keyname' => $group . '.' . $action,
            'group' => $group,
        ], [
            'description' => $description,
            'priority' => $latest ? $latest->tier + 1 : 0,
        ]);
    }

    public function permissionGroup($group, $permissions = [])
    {
        $index = 0;

        foreach ($permissions as $permisison => $description) {
            Permission::firstOrCreate([
                'keyname' => $group . '.' . $permisison,
                'group' => $group,
            ], [
                'description' => $description,
                'priority' => $index,
            ]);

            $index++;
        }
    }

    public function resourcePermissions($group)
    {
        $this->permissionGroup($group, [
            'index' => 'rapture::permission.index',
            'create' => 'rapture::permission.create',
            'edit' => 'rapture::permission.edit',
            'destroy' => 'rapture::permission.destroy',
        ]);
    }
}
