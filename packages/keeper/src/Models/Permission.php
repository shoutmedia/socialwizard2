<?php

namespace Rapture\Keeper\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function groupName()
    {
        return strtoupper(implode(' - ', explode('.', $this->group)));
    }
}
