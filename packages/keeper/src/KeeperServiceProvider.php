<?php

namespace Rapture\Keeper;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Rapture\Keeper\Facades\Keeper;
use Rapture\Keeper\Models\Permission;

class KeeperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/keeper.php' => config_path('keeper.php'),
            ], 'config');
        }

        if (!$this->app->runningInConsole() && Schema::hasTable('permissions')) {
            $this->registerPermissions();
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/keeper.php', 'keeper');

        $this->app->singleton('keeper', function ($app) {
            return new PermissionManager($app);
        });

        $this->app->singleton('keeper.driver', function ($app) {
            return $app['keeper']->driver();
        });
    }

    public function registerPermissions()
    {
        $permissions = Permission::all();

        $permissions->each(function ($item) {
            Gate::define($item->keyname, function ($user) use ($item) {
                return Keeper::allows($user, $item->keyname);
            });
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['keeper', 'keeper.driver'];
    }
}
